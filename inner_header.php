<?php
ob_start();
session_start();
if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == '') {
    header("Location:index.php");
}
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Admin Panel</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <!-- Bootstrap core CSS     -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />

        <!--  Light Bootstrap Table core CSS    -->
        <link href="css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>

        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="css/demo.css" rel="stylesheet" />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="css/pe-icon-7-stroke.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="datatables/datatables.min.css"/>
        
    </head>
    <body>

        <div class="wrapper">
            <div class="sidebar" data-color="purple" data-image="images/sidebar-4.jpg">

                <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->


                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="master_reports_listing.php" class="simple-text">
                            <?php
                            if (isset($_SESSION['username']) && $_SESSION['username'] != '') {
                                echo strtoupper("Hello, " . $_SESSION['username']);
                            } else {
                                echo "HELLO, GUEST";
                            }
                            ?>
                        </a>
                    </div>
                    <ul class="nav">
                        <li>
                            <a href="#">
                                <i class="pe-7s-server"></i>
                                <p>Master Table</p>
                            </a>
                            <ul class="nav-open">
                                <li class="">
                                    <a href="master_import.php">
                                        <i class="pe-7s-upload"></i>
                                        <p>Upload</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="master_reports_listing.php">
                                        <i class="pe-7s-note2"></i>
                                        <p>View/Edit/Add New</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <i class="pe-7s-graph"></i>
                                <p>Expense Table</p>
                            </a>
                            <ul class="nav-open">
                                <li class="">
                                    <a href="import.php">
                                        <i class="pe-7s-upload"></i>
                                        <p>Upload</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="reports_listing.php">
                                        <i class="pe-7s-note2"></i>
                                        <p>View/Edit/Add New</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="process_list.php">
                                <i class="pe-7s-repeat"></i>
                                <p>Process</p>
                            </a>
                        </li>
                        <li>
                            <a href="summary_table.php">
                                <i class="pe-7s-news-paper"></i>
                                <p>Summary Report</p>
                            </a>
                        </li>
                        <li>
                            <a href="detailed_table.php">
                                <i class="pe-7s-info"></i>
                                <p>Detailed Report</p>
                            </a>
                        </li>
                        <?php
                        if (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] == 1) {
                            ?>
                            <li>
                                <a href="user_manage.php">
                                    <i class="pe-7s-add-user"></i>
                                    <p>Assign User Privileges</p>
                                </a>
                            </li>
                            <li>
                                <a href="user_log.php">
                                    <i class="pe-7s-graph2"></i>
                                    <p>User Activity Log</p>
                                </a>
                            </li>
                            <li>
                                <a href="division_manage.php">
                                    <i class="pe-7s-stopwatch"></i>
                                    <p>Division & Year</p>
                                </a>
                            </li>
                            <?php
                        }
                        ?>

                        <li>
                            <a href="javascript:void(0);" onclick="logout_user();">
                                <i class="pe-7s-power"></i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#"></a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left">

                            </ul>

                        </div>
                    </div>
                </nav>
