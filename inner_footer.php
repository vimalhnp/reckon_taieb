
                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            
                        </nav>
                        <p class="copyright pull-right">
                            &copy; copyright
                        </p>
                    </div>
                </footer>

            </div>
        </div>




        <!--   Core JS Files   -->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

        <script src="js/light-bootstrap-dashboard.js?v=1.4.0"></script>
        <script src="js/main.js"></script>
        <script type="text/javascript" charset="utf8" src="datatables/datatables.min.js"></script>
    </body>
</html>
