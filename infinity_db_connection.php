<?php

header("Cache-control: private, no-cache");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Pragma: no-cache");
header("Cache: no-cahce");
ini_set('max_execution_time', 90000);
ini_set("memory_limit", -1);
date_default_timezone_set("Asia/Kolkata");

if ($_SERVER['HTTP_HOST'] == "localhost") {
    $isLocal = true;
} else {
    $isLocal = false;
}

if ($isLocal == true) {
    DEFINE('DB_SERVER', 'localhost'); // eg, localhost - should not be empty for productive servers
    DEFINE('DB_SERVER_USERNAME', 'root');
    DEFINE('DB_SERVER_PASSWORD', '');
    DEFINE('DB_DATABASE', 'taieb');
    DEFINE('SITE_FOLDER', 'taieb/');

    DEFINE("SITE_URL_REMOTE", "http://" . $_SERVER['HTTP_HOST'] . '/' . SITE_FOLDER);
} else {
    DEFINE('DB_SERVER', 'sql307.epizy.com');
    DEFINE('DB_SERVER_USERNAME', 'epiz_22189696');
    DEFINE('DB_SERVER_PASSWORD', 'park2500');
    DEFINE('DB_DATABASE', 'epiz_22189696_Eli');
    DEFINE('SITE_FOLDER', '/');
    DEFINE('SITE_URL_REMOTE', 'http://' . $_SERVER['HTTP_HOST'] . '/' . SITE_FOLDER);
}
DEFINE('ENCRYPT_KEY', 'vd@_applications_@');
DEFINE('CDN_MJ_IMAGEURL', 'https://' . $_SERVER['HTTP_HOST'] . '/' . SITE_FOLDER . 'images');

DEFINE('PAGINATION_LIMIT', 50);
DEFINE('ORDER_PAGINATION_LIMIT', 100);

DEFINE("SITE_ROOT", $_SERVER['DOCUMENT_ROOT'] . '/' . SITE_FOLDER);

$conn = mysqli_connect(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD, DB_DATABASE) or die('Error connecting to database.');
$conn->set_charset("utf8");

function send_mail_with_headers($subject, $from, $recipient, $mess, $isHTML = FALSE, $filearr = Array(), $path = "", $replyto = '') {
    //set the message content type
    $content_type = "text/plain";
    if ($isHTML == TRUE) {
        $content_type = "text/html";
    }
    if (is_array($recipient)) {
        $to = $recipient[0];
        $cc = $recipient[1];
        $bcc = $recipient[2];
    } else {
        $to = $recipient;
        $cc = "";
        $bcc = "";
    }
    //set the header
    $headers = "";
    if (count($filearr) > 0) {// USE multipart mime message to send mail with attachment
        //unique mime boundry seperater
        $mime_boundary_value = md5(uniqid(time()));
        //set the headers
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: multipart/mixed; boundary=\"$mime_boundary_value\";\r\n";
        $headers .= "If you are reading this, then upgrade your e-mail client to support MIME.\r\n";
        //set the message
        if ($mess <> "") {
            $mess = "--$mime_boundary_value\n" .
                    "Content-Type: $content_type; charset=\"iso-8859-1\"\n" .
                    "Content-Transfer-Encoding: 7bit\n\n" .
                    $mess . "\n\n";
        }
        for ($i = 0; $i < count($filearr); $i++) {
            // if the upload succeded, the file will exist
            if (file_exists($filearr[$i])) {
                $mess .= "--$mime_boundary_value\n";
                $mess .= "Content-Type: application/pdf; name=\"{$filearr[$i]}\"\n";
                $mess .= "Content-Disposition: attachment; filename=\"{$filearr[$i]}\"\n";
                $mess .= "Content-Transfer-Encoding: base64\n\n";
                //read file data
                $file = fopen($filearr[$i], 'rb');
                $data = fread($file, filesize($filearr[$i]));
                fclose($file);
                //encode file data
                $data = chunk_split(base64_encode($data));
                $mess .= $data . "\n\n";
            }
        }
        $mess .= "--$mime_boundary_value--\n"; //end message
    } else {
        //$headers.= 'MIME-Version: 1.0' . "\r\n";
        $headers.='Content-Type: ' . $content_type . '; charset=iso-8859-1' . "\r\n";
    }
    //    $headers.='To: ' . $to . "\r\n";
    $headers.='From: ' . $from . "\r\n";
    $bcc != '' ? $headers.="Bcc: $bcc \r\n" : '';
    $cc != '' ? $headers.="cc: $cc \r\n" : '';
    $headers.='Bounce-to: ' . $recipient[2] . "\r\n";
    if (trim($replyto) != "") {
        $headers.='Reply-To: ' . $replyto . "\r\n";
        $headers.='Return-Path: ' . $replyto . "\r\n";
    }
    //    $fp = fopen("maillog.txt", "a");
    //    fwrite($fp, "Header start here: \n" . $headers . "\n Message start here: \n" . $mess);
    //    fclose($fp);
    $response = mail($to, $subject, $mess, $headers, '-f' . $from); //this function will send mail
    return $response;
    // return "Recipient : ".$recipient."<hr>Subject: ".$subject."<hr>Message: ".$mess."<hr>Header: ".$headers;
}

//ob_end_flush();
?>