<?php
ob_start();
//session_start();
include_once './inner_header.php';
include_once './db_connection.php';

global $conn;

if ($_SESSION['is_admin'] != 1) {
    header("Location:master_reports_listing.php");
}
is_user_active();
set_user_active_time();

$basic_data = [];
$res_data = mysqli_query($conn, "SELECT ul.*, u.username FROM `user_log` AS ul JOIN `user` AS u ON (ul.user_id = u.user_id) WHERE ul.`status` = 1 ORDER BY ul.added_on DESC LIMIT 100");
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <h4 class="title">List of User Activity</h4>
                            <p class="category">Last 100 User Activity</p>
                        </div>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover ">
                            <thead>
                            <th>ID</th>
                            <th>Username</th>
                            <th>Activity</th>
                            <th>Date</th>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                if (mysqli_num_rows($res_data) > 0) {
                                    while ($data_row = mysqli_fetch_assoc($res_data)) {
                                        ?>
                                        <tr id="record_<?php echo $data_row['user_log_id']; ?>">
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo $data_row['username']; ?></td>
                                            <td><?php echo $data_row['activity']; ?></td>
                                            <td><?php echo date('j M Y, H:i:s',  strtotime($data_row['added_on'])); ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once './inner_footer.php';
