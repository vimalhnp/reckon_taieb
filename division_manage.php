<?php
ob_start();

include_once './inner_header.php';
include_once './db_connection.php';

global $conn;
if ($_SESSION['is_admin'] != 1) {
    header("Location:master_reports_listing.php");
}
is_user_active();
set_user_active_time();

$res_data = mysqli_query($conn, "SELECT * FROM `division` WHERE `status`=1 ORDER BY division_id DESC");
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add New Division</h4>
                    </div>
                    <div class="content">
                        <form class="form-horizontal" action="functions.php" method="post">
                            <input type="hidden" name="action" value="add_division">
                            <div class="row" style="margin: 0;">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Division</label>
                                        <input type="text" class="form-control" id="division" name="division" placeholder="Enter Division (4 Characters in Capital)" required="" autofocus="" />
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <button type="submit" class="btn btn-info btn-fill ">Add Division</button>
                                <!--<a href="division_manage.php" class="btn btn-default ">Cancel</a>-->
                            </div>

                            <div class="clearfix"></div>
                        </form>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover ">
                            <thead>
                            <th>ID</th>
                            <th>Division</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                if (mysqli_num_rows($res_data) > 0) {
                                    while ($data_row = mysqli_fetch_assoc($res_data)) {
                                        ?>
                                        <tr id="record_<?php echo $data_row['division_id']; ?>">
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo $data_row['division']; ?></td>
                                            <td>
                                                <a href="period_manage.php?did=<?php echo base64_encode($data_row['division_id']); ?>" title="Manage Period(s)" class="btn btn-xs btn-primary"><i class="fa fa-clock-o"></i> Manage Periods</a>
                                                <a href="javascript:void(0);" onclick="deleteDivision(<?php echo ($data_row['division_id']); ?>)" title="Delete Division" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once './footer.php';
?>