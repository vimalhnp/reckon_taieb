<?php

ob_start();
session_start();

header("Cache-control: private, no-cache");
header("Pragma: no-cache");
header("Cache: no-cahce");
ini_set('max_execution_time', 90000);
ini_set("memory_limit", -1);
//header("Content-length: 500M");
ini_set('auto_detect_line_endings', TRUE);

require_once './db_connection.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

//use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

extract($_REQUEST);
global $conn;

if (isset($action) && $action == "login_user") {
    $sel_email = mysqli_query($conn, "SELECT * FROM user WHERE username = '" . mysqli_real_escape_string($conn, $username) . "' AND `password`='" . mysqli_real_escape_string($conn, md5($password)) . "' AND `status`=1");
    if (mysqli_num_rows($sel_email) > 0) {
        $data = mysqli_fetch_assoc($sel_email);

        $_SESSION['user_id'] = $data['user_id'];
        $_SESSION['username'] = $data['username'];
        $_SESSION['email_id'] = $data['email_id'];
        $_SESSION['is_admin'] = $data['is_admin'];
        set_user_active_time();
        user_log($_SESSION['user_id'], 'logged in');
        echo 'success';
    } else {
        echo 'fail';
    }
} else if (isset($action) && $action == "logout_user") {
    user_log($_SESSION['user_id'], 'logged out');
    $_SESSION['user_id'] = 0;
    $_SESSION['username'] = '';
    $_SESSION['email_id'] = '';
    $_SESSION['is_admin'] = 0;
    unset($_SESSION['user_id']);
    unset($_SESSION['username']);
    unset($_SESSION['email_id']);
    unset($_SESSION['is_admin']);

    echo 'success';
} elseif (isset($action) && $action == "register_user") {
    $sel_email = mysqli_query($conn, "SELECT * FROM user WHERE username = '" . mysqli_real_escape_string($conn, $username) . "'");
    if (mysqli_num_rows($sel_email) > 0) {
        echo 'Already Username Exist';
    } else {
        $current_date = date('Y-m-d H:i:s');
        $insert = "INSERT INTO user (`username`,`password`,`email_id`,`added_on`,`modified_on`) VALUES ('" . $username . "','" . md5($password) . "','" . $email_id . "','" . $current_date . "','" . $current_date . "')";
        $result = mysqli_query($conn, $insert);
        if ($result) {
            echo 'Successful Registration';
        }
    }
} elseif (isset($action) && $action == "add_report") {
    $current_date = date('Y-m-d H:i:s');
    $insert = "INSERT INTO reports (`date`,`ref`,`account`,`payee`,`memo`,`amount`,`exp_head`,`added_on`,`modified_on`,`division`,`year`) VALUES ('" . mysqli_real_escape_string($conn, $date) . "','" . mysqli_real_escape_string($conn, $ref) . "','" . mysqli_real_escape_string($conn, $account) . "','" . mysqli_real_escape_string($conn, strtoupper($payee)) . "','" . mysqli_real_escape_string($conn, $memo) . "','" . mysqli_real_escape_string($conn, $amount) . "','" . mysqli_real_escape_string($conn, $exp_head) . "','" . $current_date . "','" . $current_date . "','" . mysqli_real_escape_string($conn, $division) . "','" . mysqli_real_escape_string($conn, $year) . "')";
    $result = mysqli_query($conn, $insert);
    if ($result) {
        header("Location:reports_listing.php");
    }
} elseif (isset($action) && $action == "edit_report") {
    $current_date = date('Y-m-d H:i:s');
    $reports_id = $_POST['reports_id'];
    unset($_POST['reports_id']);
    unset($_POST['action']);
    $updateQ = '';
    if (count($_POST) > 0) {
        foreach ($_POST as $field_name => $field_value) {
            if ($field_name == 'payee') {
                $field_value = strtoupper($field_value);
            }
            $updateQ.='`' . $field_name . '` ="' . mysqli_real_escape_string($conn, $field_value) . '", ';
        }
    }
    $updateQ = rtrim($updateQ, ", ");

    $update = "UPDATE `reports` SET $updateQ WHERE reports_id = '" . mysqli_real_escape_string($conn, $reports_id) . "'";
    $result = mysqli_query($conn, $update);
    if ($result) {
        user_log($_SESSION['user_id'], 'edited records in the Expense table');
        header("Location:reports_listing.php?success");
    } else {
        header("Location:reports_listing.php?fail");
    }
} elseif (isset($action) && $action == "upload_file") {
    $target_dir = "uploads/";
    $target_file = $target_dir . time() . basename($_FILES["report_file"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


// Check file size
    if ($_FILES["report_file"]["size"] > 5000000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
// Allow certain file formats
    if ($imageFileType != "xlsx") {
        echo "Sorry, only Excel (.xlsx) file allowed.";
        $uploadOk = 0;
    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["report_file"]["tmp_name"], $target_file)) {
            basename($_FILES["report_file"]["name"]);
            require 'vendor/autoload.php';


            $reader = new Xlsx();
            $spreadsheet = $reader->load($target_file);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();


            $headings = array_shift($sheetData);
            //convert headings to small case and remove space
            array_walk($headings, function(&$val) {
                $val = str_replace(' ', '_', strtolower($val));
            });

            // combine all data so key as value setup
            array_walk($sheetData, function (&$row) use ($headings) {
                $row = array_combine($headings, $row);
            }
            );
            $current_date = date('Y-m-d H:i:s');
            $count_updated = 0;
            if ($import_action == 'replace') {
                mysqli_query($conn, "DELETE FROM reports WHERE division = '" . $division . "' AND `year` = '" . $year . "'");
            }
            foreach ($sheetData as $key => $value) {
                $value1 = array_values($value);
                $value1 = array_filter($value1);

                if (count($value1) > 0) {
//                    if ($import_action == 'append') {

                        $q = "INSERT INTO reports SET ";

                        foreach ($value as $fields => $data) {
                            if ($fields == 'payee') {
                                $data = strtoupper($data);
                            }
                            if ($fields != '') {
                                if ($fields != 'payee') {
                                    $data = trim(htmlspecialchars($data, ENT_IGNORE));
                                }
                                $q.="`$fields` = '" . mysqli_real_escape_string($conn, $data) . "',";
                            }
                        }
                        $q.="`division`='" . mysqli_real_escape_string($conn, trim($division)) . "',";
                        $q.="`year`='" . mysqli_real_escape_string($conn, trim($year)) . "',";
                        $q.="`added_on` = '" . $current_date . "',";
                        $q.="`modified_on` = '" . $current_date . "'";
                        $q = rtrim($q, ",") . ";";
                        $result = mysqli_query($conn, $q);
                        $numrow = mysqli_affected_rows($conn);
                        if ($numrow) {
                            $count_updated++;
                        }
                    /* } elseif ($import_action == 'replace') {
                        $q = "UPDATE reports SET ";
                        $where = ' WHERE ';
                        $whereArr = [];
                        foreach ($value as $fields => $data) {
                            if ($fields == 'payee') {
                                $data = strtoupper($data);
                                $whereArr[] = " `payee`='" . mysqli_real_escape_string($conn, $data) . "'";
                            }
                            if ($fields == 'date') {
                                $whereArr[] = " `date`='" . mysqli_real_escape_string($conn, $data) . "'";
                            }
                            if ($fields != '' && $fields != 'payee' && $fields != 'date') {
                                $q.="`$fields` = '" . mysqli_real_escape_string($conn, trim(htmlspecialchars($data, ENT_IGNORE))) . "',";
                            }
                        }
                        $where.= implode(" AND ", $whereArr);
                        $where .=" AND `division`='" . mysqli_real_escape_string($conn, trim($division)) . "' ";
                        $where .=" AND `year`='" . mysqli_real_escape_string($conn, trim($year)) . "' ";

                        $q.="`modified_on` = '" . $current_date . "'";
                        $q = rtrim($q, ",");
                        $q = $q . $where;

                        $result = mysqli_query($conn, $q);
                        $result = mysqli_query($conn, $q);
                        $numrow = mysqli_affected_rows($conn);
                        if ($numrow) {
                            $count_updated++;
                        }
                      } */
                    }
                }
            user_log($_SESSION['user_id'], 'uploaded records in Expense table');
            header("Location:reports_listing.php?n=" . base64_encode($count_updated));
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
} elseif (isset($action) && $action == "master_upload_file") {
    $target_dir = "uploads/";
    $target_file = $target_dir . time() . basename($_FILES["report_file"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


// Check file size
    if ($_FILES["report_file"]["size"] > 5000000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
// Allow certain file formats
    if ($imageFileType != "xlsx") {
        echo "Sorry, only Excel (.xlsx) file allowed.";
        $uploadOk = 0;
    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["report_file"]["tmp_name"], $target_file)) {
            basename($_FILES["report_file"]["name"]);
            require 'vendor/autoload.php';


            $reader = new Xlsx();
            $spreadsheet = $reader->load($target_file);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();


            $headings = array_shift($sheetData);
            //convert headings to small case and remove space
            array_walk($headings, function(&$val) {
                $val = str_replace(' ', '_', strtolower($val));
            });

            // combine all data so key as value setup
            array_walk($sheetData, function (&$row) use ($headings) {
                $row = array_combine($headings, $row);
            }
            );
            $current_date = date('Y-m-d H:i:s');
            $count_updated = 0;
            if ($import_action == 'replace') {
                mysqli_query($conn, "DELETE FROM master_reports WHERE division = '" . $division . "'");
            }

            foreach ($sheetData as $key => $value) {
                $value1 = array_values($value);
                $value1 = array_filter($value1);

                if (count($value1) > 0) {
//                    if ($import_action == 'append') {

                        $q = "INSERT INTO master_reports SET ";

                        foreach ($value as $fields => $data) {
                            if ($fields == 'payee') {
                                $data = strtoupper($data);
                            }
                            if ($fields != '') {
                                if ($fields != 'payee') {
                                    $data = trim(htmlspecialchars($data, ENT_IGNORE));
                                }
                                $q.="`$fields` = '" . mysqli_real_escape_string($conn, $data) . "',";
                            }
                        }
                        $q.="`division`='" . mysqli_real_escape_string($conn, trim($division)) . "',";
//                        $q.="`year`='" . mysqli_real_escape_string($conn, trim($year)) . "',";
                        $q.="`added_on` = '" . $current_date . "',";
                        $q.="`modified_on` = '" . $current_date . "'";
                        $q = rtrim($q, ",") . ";";
                        $result = mysqli_query($conn, $q);
                        $numrow = mysqli_affected_rows($conn);
                        if ($numrow) {
                            $count_updated++;
                        }
                    /* } elseif ($import_action == 'replace') {
                        $q = "UPDATE master_reports SET ";
                        $where = ' WHERE ';
                        foreach ($value as $fields => $data) {
                            if ($fields == 'payee') {
                                $data = strtoupper($data);
                                $where .=" `payee`='" . mysqli_real_escape_string($conn, $data) . "'";
                            }
                            if ($fields != '' && $fields != 'payee') {
                                $q.="`$fields` = '" . mysqli_real_escape_string($conn, trim(htmlspecialchars($data, ENT_IGNORE))) . "',";
                            }
                        }
                        $where .=" AND `division`='" . mysqli_real_escape_string($conn, trim($division)) . "' ";
//                        $where .=" AND `year`='" . mysqli_real_escape_string($conn, trim($year)) . "' ";
                        $q.="`modified_on` = '" . $current_date . "'";
                        $q = rtrim($q, ",");
                        $q = $q . $where;
                        $result = mysqli_query($conn, $q);
                        $numrow = mysqli_affected_rows($conn);
                        if ($numrow) {
                            $count_updated++;
                        }
                      } */
                    }
                }
            user_log($_SESSION['user_id'], 'uploaded records in Master table');
            header("Location:master_reports_listing.php?n=" . base64_encode($count_updated));
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
} elseif (isset($action) && $action == "change_permission") {
    $current_date = date('Y-m-d H:i:s');
    $state1 = '0';
    if ($state == 'true') {
        $state1 = '1';
    }
    $update = "UPDATE `user` SET `$field` = '" . mysqli_real_escape_string($conn, $state1) . "' WHERE user_id='" . mysqli_real_escape_string($conn, $user_id) . "'";
    $result = mysqli_query($conn, $update);
    echo 'success';
} elseif (isset($action) && $action == "change_permission_new") {
    $current_date = date('Y-m-d H:i:s');
    $upload_permission = $process_permission = $report_permission = '';
    if (isset($_POST['upload_permission'])) {
        $upload_permission = json_encode($_POST['upload_permission'], TRUE);
    }
    if (isset($_POST['process_permission'])) {
        $process_permission = json_encode($_POST['process_permission'], TRUE);
    }
    if (isset($_POST['report_permission'])) {
        $report_permission = json_encode($_POST['report_permission'], TRUE);
    }
    $update = "UPDATE `user` SET `upload_permission` = '" . mysqli_real_escape_string($conn, $upload_permission) . "',`process_permission` = '" . mysqli_real_escape_string($conn, $process_permission) . "',`report_permission` = '" . mysqli_real_escape_string($conn, $report_permission) . "' WHERE user_id='" . mysqli_real_escape_string($conn, $user_id) . "'";

    $result = mysqli_query($conn, $update);
    $sel_email = mysqli_query($conn, "SELECT username FROM user WHERE user_id='" . mysqli_real_escape_string($conn, $user_id) . "'");
    if (mysqli_num_rows($sel_email) > 0) {
        $data = mysqli_fetch_assoc($sel_email);
        user_log($_SESSION['user_id'], 'modified the use privileges of ' . $data['username']);
    }
    echo 'success';
} elseif (isset($action) && $action == "add_user") {
    $updateQ = '';
    if (isset($password) && $password != '') {
        $updateQ.=" , `password`='" . mysqli_real_escape_string($conn, md5($password)) . "' ";
    }
    $current_date = date('Y-m-d H:i:s');
    $insert = "INSERT INTO user SET `username`='" . mysqli_real_escape_string($conn, $email_id) . "',`email_id`='" . mysqli_real_escape_string($conn, $email_id) . "',`is_admin`='0',`login_first_time`='1',`added_on`='" . mysqli_real_escape_string($conn, $current_date) . "',`modified_on`='" . mysqli_real_escape_string($conn, $current_date) . "' " . $updateQ;
    $result = mysqli_query($conn, $insert);
    if ($result) {
        user_log($_SESSION['user_id'], 'added new user');
        header("Location:user_manage.php");
    }
} elseif (isset($action) && $action == "edit_user") {
    $current_date = date('Y-m-d H:i:s');
    $user_id = $_POST['user_id'];
    unset($_POST['user_id']);
    unset($_POST['action']);
    $updateQ = '';
    if (isset($password) && $password != '') {
        $updateQ.=" `password`='" . mysqli_real_escape_string($conn, md5($password)) . "', ";
    }
    unset($_POST['password']);
    if (count($_POST) > 0) {
        foreach ($_POST as $field_name => $field_value) {
            $updateQ.='`' . $field_name . '` ="' . mysqli_real_escape_string($conn, $field_value) . '", ';
        }
    }

    $updateQ = rtrim($updateQ, ", ");

    $update = "UPDATE `user` SET $updateQ WHERE user_id = '" . mysqli_real_escape_string($conn, $user_id) . "'";
    $result = mysqli_query($conn, $update);
    if ($result) {
        header("Location:user_manage.php?success");
    } else {
        header("Location:user_manage.php?fail");
    }
} elseif (isset($action) && $action == "delete_user") {
    $current_date = date('Y-m-d H:i:s');
    $user_id = $_POST['user_id'];
    $update = "UPDATE `user` SET `status`=0 WHERE user_id = '" . mysqli_real_escape_string($conn, $user_id) . "'";
    $result = mysqli_query($conn, $update);
    if ($result) {
        user_log($_SESSION['user_id'], 'deleted user : ' . $user_id);
        echo 'user_manage.php?success';
    } else {
        echo 'user_manage.php?fail';
    }
} elseif (isset($action) && $action == "add_master_report") {
    $current_date = date('Y-m-d H:i:s');
    $insert = "INSERT INTO master_reports (`payee`,`exp_head`,`added_on`,`modified_on`,`division`) VALUES ('" . mysqli_real_escape_string($conn, strtoupper($payee)) . "','" . mysqli_real_escape_string($conn, $exp_head) . "','" . $current_date . "','" . $current_date . "','" . mysqli_real_escape_string($conn, $division) . "')";
    $result = mysqli_query($conn, $insert);
    if ($result) {
        header("Location:master_reports_listing.php");
    }
} elseif (isset($action) && $action == "edit_master_report") {
    $current_date = date('Y-m-d H:i:s');
    $reports_id = $_POST['master_reports_id'];
    unset($_POST['master_reports_id']);
    unset($_POST['action']);
    $updateQ = '';
    if (count($_POST) > 0) {
        foreach ($_POST as $field_name => $field_value) {
            if ($field_name == 'payee') {
                $field_value = strtoupper($field_value);
            }
            $updateQ.='`' . $field_name . '` ="' . mysqli_real_escape_string($conn, $field_value) . '", ';
        }
    }
    $updateQ = rtrim($updateQ, ", ");

    $update = "UPDATE `master_reports` SET $updateQ WHERE master_reports_id = '" . mysqli_real_escape_string($conn, $reports_id) . "'";
    $result = mysqli_query($conn, $update);
    if ($result) {
        user_log($_SESSION['user_id'], 'edited records in the Master table');
        header("Location:master_reports_listing.php?success");
    } else {
        header("Location:master_reports_listing.php?fail");
    }
} elseif (isset($action) && $action == "process_expence_data") {
    $current_date = date('Y-m-d H:i:s');



    $getreports = mysqli_query($conn, "SELECT * FROM `reports` WHERE `exp_head` ='' AND  `status`=1");
    if (mysqli_num_rows($getreports) > 0) {

        //get master data into array 
        $masterdataArr = [];
        $getmasterreports = mysqli_query($conn, "SELECT * FROM `master_reports` WHERE `status` = 1");
        if (mysqli_num_rows($getmasterreports) > 0) {
            while ($masterdata = mysqli_fetch_assoc($getmasterreports)) {
                $masterdataArr[$masterdata['payee']] = $masterdata['exp_head'];
            }
        }
        $entered_masterdata = [];
        while ($data = mysqli_fetch_assoc($getreports)) {
            if (key_exists($data['payee'], $masterdataArr)) {
                // update expense table from master table
                $update = "UPDATE `reports` SET `exp_head` ='" . $masterdataArr[$data['payee']] . "' WHERE reports_id = '" . mysqli_real_escape_string($conn, $data['reports_id']) . "'";
                $result = mysqli_query($conn, $update);
            } else {
                if (!in_array($data['payee'], $entered_masterdata)) {
                    //enter data into master table
                    $insert = "INSERT INTO master_reports (`payee`,`added_on`,`modified_on`,`division`) VALUES ('" . mysqli_real_escape_string($conn, strtoupper($data['payee'])) . "','" . $current_date . "','" . $current_date . "','" . mysqli_real_escape_string($conn, strtoupper($data['division'])) . "')";
                    $result = mysqli_query($conn, $insert);
                    $entered_masterdata[] = $data['payee'];
                }
            }
        }
    }


    if ($result) {
        user_log($_SESSION['user_id'], 'ran the process');
        header("Location:process_list.php?success");
    } else {
        header("Location:process_list.php?fail");
    }
} elseif (isset($action) && $action == "export_summary_data") {

    $getmasterreports = mysqli_query($conn, "SELECT exp_head,amount,account FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "'");
    $creditArr = $creditArr2 = [];  //  If it is negative, it is identified as the credit balance
    $debitArr = $debitArr2 = [];   //  If it is positive, it is identified as the debit balance
    $debitTotal = 0;
    $creditTotal = 0;
    if (mysqli_num_rows($getmasterreports) > 0) {
        $file_name = $division . $year;
        while ($masterdata = mysqli_fetch_assoc($getmasterreports)) {
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($amt < 0) {
                $debitArr[$masterdata['exp_head']][] = abs($amt);
                $creditArr2[$masterdata['account']][] = abs($amt);
//                $creditTotal+=abs($amt);
            } else {
                $creditArr[$masterdata['exp_head']][] = abs($amt);
                $debitArr2[$masterdata['account']][] = abs($amt);
//                $debitTotal+=abs($amt);
            }
        }
    } else {
        echo 'summary_table.php?fail';
        exit;
    }
    $getexpheads = mysqli_query($conn, "SELECT DISTINCT exp_head FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `account`!=''  AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY exp_head, date, amount");
    $table_exp = '<table cellpadding="5" cellmargin="5" style="width:100%;border:1px solid black;">';
    $table_exp.='<tr class="border_bottom"><td colspan="4">Summary report for ' . $file_name . '</td></tr>';
    $table_exp.='<tr class="border_bottom"><td>S No</td><td>Exp Head</td><td>Debit</td><td>Credit</td></tr>';
    $i = 1;
    while ($exp_heads = mysqli_fetch_assoc($getexpheads)) {
        $debitAmt = (isset($debitArr[$exp_heads['exp_head']]) && count($debitArr[$exp_heads['exp_head']]) > 0) ? array_sum($debitArr[$exp_heads['exp_head']]) : "-";
        $creditAmt = (isset($creditArr[$exp_heads['exp_head']]) && count($creditArr[$exp_heads['exp_head']]) > 0) ? array_sum($creditArr[$exp_heads['exp_head']]) : "-";
        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($debitAmt - $creditAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $debitAmt = '-';
                $creditAmt = abs($diff);
            } else {
                $creditAmt = '-';
                $debitAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);

        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $table_exp.='<tr>';
        $table_exp.='<td>' . $i . '</td>';
        $table_exp.='<td>' . $exp_heads['exp_head'] . '</td>';
        $table_exp.='<td>' . $debitAmt . '</td>';
        $table_exp.='<td>' . $creditAmt . '</td>';
        $table_exp.='</tr>';
        $i++;
    }

    $getaccount = mysqli_query($conn, "SELECT DISTINCT account FROM `reports` WHERE `status` = 1 AND `account`!='' AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY account");
    while ($accounts = mysqli_fetch_assoc($getaccount)) {
        $creditAmt = (isset($debitArr2[$accounts['account']]) && count($debitArr2[$accounts['account']]) > 0) ? array_sum($debitArr2[$accounts['account']]) : "-";
        $debitAmt = (isset($creditArr2[$accounts['account']]) && count($creditArr2[$accounts['account']]) > 0) ? array_sum($creditArr2[$accounts['account']]) : "-";

        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($creditAmt - $debitAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $creditAmt = '-';
                $debitAmt = abs($diff);
            } else {
                $debitAmt = '-';
                $creditAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);
        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $table_exp.='<tr>';
        $table_exp.='<td>' . $i . '</td>';
        $table_exp.='<td>' . $accounts['account'] . '</td>';
        $table_exp.='<td>' . ($debitAmt) . '</td>';
        $table_exp.='<td>' . ($creditAmt) . '</td>';
        $table_exp.='</tr>';
        $i++;
    }
    if (abs($debitTotal) > 0) {
        $debitTotal = "$ " . number_format($debitTotal, 2);
    }
    if (abs($creditTotal) > 0) {
        $creditTotal = "$ " . number_format($creditTotal, 2);
    }
    $table_exp.='<tr><td></td><td></td><td><hr/></td><td><hr/></td></tr>';
    $table_exp.='<tr>';
    $table_exp.='<td> </td>';
    $table_exp.='<td>Total</td>';
    $table_exp.='<td>' . $debitTotal . '</td>';
    $table_exp.='<td>' . $creditTotal . '</td>';
    $table_exp.='</tr>';
    $table_exp.='<tr><td></td><td></td><td><hr/></td><td><hr/></td></tr>';
    $table_exp.='</table>';

    $html = '<html>
                <head>
                    <style>
                        @page {
                            margin-header: 10mm;
                            margin-footer: 10mm;
                            margin:15px;
                        }
                        body {font-family: sans-serif;
                              font-size: 10pt;
                        }
                        p {	margin: 0pt; }
                        .border_bottom td{ border-bottom:1px solid black; }
                    </style>
                </head>
                <body style="background-color: #eaeaea;">
                    <!--mpdf
                    <htmlpagefooter name="myfooter">
                    <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
                    Page {PAGENO} of {nb}
                    </div>
                    </htmlpagefooter>
                    <sethtmlpagefooter name="myfooter" value="off" />
                    mpdf-->
                    <div style="width: 100%; margin: 0; padding: 0;">
                        <div style="background-color:#fff;width:100%;">
                            ' . $table_exp . '
                        </div>
                    </div>
                </body>
            </html>';


    define('_MPDF_PATH', './mpdf60/');
    include("./mpdf60/mpdf.php");

    $mpdf = new mPDF('d', 'A4', '', '', 0, 0, 0, 0, 5, 5);
    $mpdf->SetProtection(array('print'));
    $mpdf->SetTitle($file_name . " " . date('Y-m-d'));
    $mpdf->SetAuthor("Vimal Patel");
    $mpdf->WriteHTML($html);
    $pdffile = $file_name . ".pdf";
//    $mpdf->Output();

    $mpdf->Output(PDF_PATH . $pdffile);
//    header("Location:" . PDF_PATH . $pdffile);
    user_log($_SESSION['user_id'], 'printed the summary report (pdf)');
    echo PDF_PATH . $pdffile;
    exit();
} elseif (isset($action) && $action == "export_summary_data_excel") {

    require 'vendor/autoload.php';

    $spreadsheet = new Spreadsheet();  /* ----Spreadsheet object----- */
    $Excel_writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);  /* ----- Excel (Xls) Object */
    $spreadsheet->setActiveSheetIndex(0);
    $activeSheet = $spreadsheet->getActiveSheet();


    $getmasterreports = mysqli_query($conn, "SELECT exp_head,amount,account FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "'");
    $creditArr = $creditArr2 = [];  //  If it is negative, it is identified as the credit balance
    $debitArr = $debitArr2 = [];   //  If it is positive, it is identified as the debit balance
    $debitTotal = 0;
    $creditTotal = 0;

    if (mysqli_num_rows($getmasterreports) > 0) {
        $file_name = $division . $year;
        while ($masterdata = mysqli_fetch_assoc($getmasterreports)) {
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($amt < 0) {
                $debitArr[$masterdata['exp_head']][] = abs($amt);
                $creditArr2[$masterdata['account']][] = abs($amt);
//                $creditTotal+=abs($amt);
            } else {
                $creditArr[$masterdata['exp_head']][] = abs($amt);
                $debitArr2[$masterdata['account']][] = abs($amt);
//                $debitTotal+=abs($amt);
            }
        }
    } else {
        echo 'summary_table.php?fail';
        exit;
    }
    $getexpheads = mysqli_query($conn, "SELECT DISTINCT exp_head FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `account`!=''  AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY exp_head, date, amount");
    $activeSheet->setCellValue('A1', 'S No')->getStyle('A1')->getFont()->setBold(true);
    $activeSheet->setCellValue('B1', 'Exp Head')->getStyle('B1')->getFont()->setBold(true);
    $activeSheet->setCellValue('C1', 'Debit')->getStyle('C1')->getFont()->setBold(true);
    $activeSheet->setCellValue('D1', 'Credit')->getStyle('D1')->getFont()->setBold(true);
    $i = 1;
    $row = 2;
    while ($exp_heads = mysqli_fetch_assoc($getexpheads)) {
        $debitAmt = (isset($debitArr[$exp_heads['exp_head']]) && count($debitArr[$exp_heads['exp_head']]) > 0) ? array_sum($debitArr[$exp_heads['exp_head']]) : "-";
        $creditAmt = (isset($creditArr[$exp_heads['exp_head']]) && count($creditArr[$exp_heads['exp_head']]) > 0) ? array_sum($creditArr[$exp_heads['exp_head']]) : "-";
        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($debitAmt - $creditAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $debitAmt = '-';
                $creditAmt = abs($diff);
            } else {
                $creditAmt = '-';
                $debitAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);

        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $activeSheet->setCellValue('A' . $row, $i);
        $activeSheet->setCellValue('B' . $row, $exp_heads['exp_head']);
        $activeSheet->setCellValue('C' . $row, $debitAmt);
        $activeSheet->setCellValue('D' . $row, $creditAmt);
        $row++;
        $i++;
    }

    $getaccount = mysqli_query($conn, "SELECT DISTINCT account FROM `reports` WHERE `status` = 1 AND `account`!='' AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY account");
    while ($accounts = mysqli_fetch_assoc($getaccount)) {
        $creditAmt = (isset($debitArr2[$accounts['account']]) && count($debitArr2[$accounts['account']]) > 0) ? array_sum($debitArr2[$accounts['account']]) : "-";
        $debitAmt = (isset($creditArr2[$accounts['account']]) && count($creditArr2[$accounts['account']]) > 0) ? array_sum($creditArr2[$accounts['account']]) : "-";

        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($creditAmt - $debitAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $creditAmt = '-';
                $debitAmt = abs($diff);
            } else {
                $debitAmt = '-';
                $creditAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);
        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $activeSheet->setCellValue('A' . $row, $i);
        $activeSheet->setCellValue('B' . $row, $accounts['account']);
        $activeSheet->setCellValue('C' . $row, $debitAmt);
        $activeSheet->setCellValue('D' . $row, $creditAmt);
        $row++;
        $i++;
    }
    if (abs($debitTotal) > 0) {
        $debitTotal = "$ " . number_format($debitTotal, 2);
    }
    if (abs($creditTotal) > 0) {
        $creditTotal = "$ " . number_format($creditTotal, 2);
    }

    $activeSheet->setCellValue('A' . $row, '');
    $activeSheet->setCellValue('B' . $row, 'Total');
    $activeSheet->setCellValue('C' . $row, $debitTotal)->getStyle('C' . $row)->getFont()->setBold(true);
    $activeSheet->setCellValue('D' . $row, $creditTotal)->getStyle('D' . $row)->getFont()->setBold(true);



    $filename = 'Summary report for ' . $file_name;
//    header('Content-Type: application/vnd.ms-excel');
//    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"'); /* -- $filename is  xsl filename --- */
//    header('Cache-Control: max-age=0');
    $Excel_writer->save('uploads/' . $filename . '.xlsx');
    user_log($_SESSION['user_id'], 'printed the summary report (excel)');
    echo 'uploads/' . $filename . '.xlsx';
} elseif (isset($action) && $action == "export_detailed_data") {

    /* GET SUMMARY DATA :: START */
    $getmasterreports = mysqli_query($conn, "SELECT exp_head,amount,account FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "'");
    $creditArr = $creditArr2 = [];  //  If it is negative, it is identified as the credit balance
    $debitArr = $debitArr2 = [];   //  If it is positive, it is identified as the debit balance
    $debitTotal = 0;
    $creditTotal = 0;
    if (mysqli_num_rows($getmasterreports) > 0) {
        $file_name = $division . $year;
        while ($masterdata = mysqli_fetch_assoc($getmasterreports)) {
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($amt < 0) {
                $debitArr[$masterdata['exp_head']][] = abs($amt);
                $creditArr2[$masterdata['account']][] = abs($amt);
//                $creditTotal+=abs($amt);
            } else {
                $creditArr[$masterdata['exp_head']][] = abs($amt);
                $debitArr2[$masterdata['account']][] = abs($amt);
//                $debitTotal+=abs($amt);
            }
        }
    } else {

        echo 'detailed_table.php?fail';
        exit;
    }
    $getexpheads = mysqli_query($conn, "SELECT DISTINCT exp_head FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `account`!=''  AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY exp_head, date, amount");
    $table_exp = '<table cellpadding="5" cellmargin="5" style="width:100%;border:1px solid black;">';
    $table_exp.='<tr class="border_bottom"><td colspan="4"><b>Summary Report for ' . $file_name . '</b></td></tr>';
    $table_exp.='<tr class="border_bottom"><td><b>S No</b></td><td><b>Exp Head/Account</b></td><td><b>Debit</b></td><td><b>Credit</b></td></tr>';
    $i = 1;
    while ($exp_heads = mysqli_fetch_assoc($getexpheads)) {
        $debitAmt = (isset($debitArr[$exp_heads['exp_head']]) && count($debitArr[$exp_heads['exp_head']]) > 0) ? array_sum($debitArr[$exp_heads['exp_head']]) : "-";
        $creditAmt = (isset($creditArr[$exp_heads['exp_head']]) && count($creditArr[$exp_heads['exp_head']]) > 0) ? array_sum($creditArr[$exp_heads['exp_head']]) : "-";
        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($debitAmt - $creditAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $debitAmt = '-';
                $creditAmt = abs($diff);
            } else {
                $creditAmt = '-';
                $debitAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);

        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $table_exp.='<tr>';
        $table_exp.='<td>' . $i . '</td>';
        $table_exp.='<td>' . $exp_heads['exp_head'] . '</td>';
        $table_exp.='<td>' . $debitAmt . '</td>';
        $table_exp.='<td>' . $creditAmt . '</td>';
        $table_exp.='</tr>';
        $i++;
    }

    $getaccount = mysqli_query($conn, "SELECT DISTINCT account FROM `reports` WHERE `status` = 1 AND `account`!='' AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY account");
    while ($accounts = mysqli_fetch_assoc($getaccount)) {
        $creditAmt = (isset($debitArr2[$accounts['account']]) && count($debitArr2[$accounts['account']]) > 0) ? array_sum($debitArr2[$accounts['account']]) : "-";
        $debitAmt = (isset($creditArr2[$accounts['account']]) && count($creditArr2[$accounts['account']]) > 0) ? array_sum($creditArr2[$accounts['account']]) : "-";

        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($creditAmt - $debitAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $creditAmt = '-';
                $debitAmt = abs($diff);
            } else {
                $debitAmt = '-';
                $creditAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);
        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $table_exp.='<tr>';
        $table_exp.='<td>' . $i . '</td>';
        $table_exp.='<td>' . $accounts['account'] . '</td>';
        $table_exp.='<td>' . ($debitAmt) . '</td>';
        $table_exp.='<td>' . ($creditAmt) . '</td>';
        $table_exp.='</tr>';
        $i++;
    }
    if (abs($debitTotal) > 0) {
        $debitTotal = "$ " . number_format($debitTotal, 2);
    }
    if (abs($creditTotal) > 0) {
        $creditTotal = "$ " . number_format($creditTotal, 2);
    }
    $table_exp.='<tr><td></td><td></td><td><hr/></td><td><hr/></td></tr>';
    $table_exp.='<tr>';
    $table_exp.='<td> </td>';
    $table_exp.='<td>Total</td>';
    $table_exp.='<td>' . $debitTotal . '</td>';
    $table_exp.='<td>' . $creditTotal . '</td>';
    $table_exp.='</tr>';
    $table_exp.='<tr><td></td><td></td><td><hr/></td><td><hr/></td></tr>';
    $table_exp.='</table><div class="page_break"></div>';
    /* GET SUMMARY DATA :: END */

    $getmasterreports = mysqli_query($conn, "SELECT * FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `account`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY exp_head, date, amount");
    $creditArr = $creditArr2 = [];  //  If it is negative, it is identified as the credit balance
    $debitArr = $debitArr2 = [];   //  If it is positive, it is identified as the debit balance
    $debitTotal = 0;
    $creditTotal = 0;
    $exp_creditTotal = 0;
    $exp_debitTotal = 0;
//    $reportsArr = [];

    if (mysqli_num_rows($getmasterreports) > 0) {
        $file_name = $division . $year;
        $table_exp .= '<table cellpadding="5" cellmargin="5" style="width:100%;border:1px solid black;">';
        $table_exp.='<tr class="border_bottom"><td colspan="8" style="background-color:#c5e1b1;"><b>Detailed Report for ' . $division . ' for ' . $year . '</b></td></tr>';
        $table_exp.='<tr class="border_bottom"><td><b>Exp Head</b></td><td><b>Ref</b></td><td><b>Payee</b></td><td class="blue_bg"><b>Account</b></td><td class="green_bg"><b>Debit</b></td><td class="green_bg"><b>Credit</b></td><td><b>Balance</b></td></tr>';
        $table_exp.='<tr class="border_bottom"><td></td><td><b>Date</b></td><td colspan="6" style="background-color:#fde59b;"><b>Memo</b></td></tr>';
        $table_exp.='<tr class="border_bottom"><td colspan="8" style="background-color:#c5e1b1;"></td></tr>';
        $currentExp = '';
        while ($masterdata = mysqli_fetch_assoc($getmasterreports)) {
//            $reportsArr[] = $masterdata;
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($currentExp != $masterdata['exp_head'] && $currentExp != '') {
//                $table_exp.='<tr ><td colspan="7" style="text-align:right;">=================</td></tr>';
                $table_exp .= '</table><div class="page_break"></div>';
                $table_exp .= '<table cellpadding="5" cellmargin="5" style="width:100%;border:1px solid black;">';
//                $table_exp.='<tr class="border_bottom"><td colspan="7" style="background-color:#c5e1b1;">Detailed Report for ' . $division . ' for ' . $year . '</td></tr>';
                $table_exp.='<tr class="border_bottom"><td><b>Exp Head</b></td><td><b>Ref</b></td><td><b>Payee</b></td><td class="blue_bg"><b>Account</b></td><td class="green_bg"><b>Debit</b></td><td class="green_bg"><b>Credit</b></td><td><b>Balance</b></td></tr>';
                $table_exp.='<tr class="border_bottom"><td></td><td><b>Date</b></td><td colspan="6" style="background-color:#fde59b;"><b>Memo</b></td></tr>';
                $table_exp.='<tr class="border_bottom"><td colspan="8" style="background-color:#c5e1b1;"></td></tr>';
                $exp_debitTotal = 0;
                $exp_creditTotal = 0;
            }
            $currentExp = $masterdata['exp_head'];
            if ($amt < 0) {
                $creditTotal+=abs($amt);
                $exp_creditTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['exp_head'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['payee'] . '</td><td class="blue_bg">' . $masterdata['account'] . '</td><td class="green_bg">-</td><td class="green_bg">' . "$ " . number_format(abs($amt), 2) . '</td><td class="">' . "$ " . number_format($diff2, 2) . '</td></tr>';
                $table_exp.='<tr class="border_bottom"><td></td><td>' . $masterdata['ref'] . '</td><td colspan="6" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';
            } else {
                $debitTotal+=abs($amt);
                $exp_debitTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['exp_head'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['payee'] . '</td><td class="blue_bg">' . $masterdata['account'] . '</td><td class="green_bg">' . "$ " . number_format(abs($amt), 2) . '</td><td class="green_bg">-</td><td class="">' . "$ " . number_format($diff2, 2) . '</td></tr>';
                $table_exp.='<tr class="border_bottom"><td></td><td>' . $masterdata['ref'] . '</td><td colspan="6" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';
            }
        }
        $table_exp .= '</table><div class="page_break"></div>';
        $table_exp .= '<table cellpadding="5" cellmargin="5" style="width:100%;border:1px solid black;">';
//        $table_exp.='<tr class="border_bottom"><td colspan="8" style="background-color:#c5e1b1;">Detailed Report for ' . $division . ' for ' . $year . '</td></tr>';
        $table_exp.='<tr class="border_bottom"><td><b>Account</b></td><td><b>Date</b></td><td><b>Payee</b></td><td class="blue_bg"><b>Exp Head</b></td><td class="green_bg"><b>Debit</b></td><td class="green_bg"><b>Credit</b></td><td><b>Balance</b></td></tr>';
        $table_exp.='<tr class="border_bottom"><td></td><td><b>Ref</b></td><td colspan="6" style="background-color:#fde59b;"><b>Memo</b></td></tr>';
        $table_exp.='<tr class="border_bottom"><td colspan="8" style="background-color:#c5e1b1;"></td></tr>';
        $getmasterreports1 = mysqli_query($conn, "SELECT * FROM `reports` WHERE `status` = 1 AND `account`!='' AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY account, date, amount");
        $exp_creditTotal = 0;
        $exp_debitTotal = 0;
        $currentExp = '';
        while ($masterdata = mysqli_fetch_assoc($getmasterreports1)) {
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($currentExp != $masterdata['account'] && $currentExp != '') {
//                $table_exp.='<tr ><td colspan="8" style="text-align:right;">=================</td></tr>';
                $table_exp .= '</table><div class="page_break"></div>';
                $table_exp .= '<table cellpadding="5" cellmargin="5" style="width:100%;border:1px solid black;">';
//                $table_exp.='<tr class="border_bottom"><td colspan="8" style="background-color:#c5e1b1;">Detailed Report for ' . $division . ' for ' . $year . '</td></tr>';
                $table_exp.='<tr class="border_bottom"><td><b>Account</b></td><td><b>Date</b></td><td><b>Payee</b></td><td class="blue_bg"><b>Exp Head</b></td><td class="green_bg"><b>Debit</b></td><td class="green_bg"><b>Credit</b></td><td><b>Balance</b></td></tr>';
                $table_exp.='<tr class="border_bottom"><td></td><td><b>Ref</b></td><td colspan="6" style="background-color:#fde59b;"><b>Memo</b></td></tr>';
                $table_exp.='<tr class="border_bottom"><td colspan="8" style="background-color:#c5e1b1;"></td></tr>';
                $exp_debitTotal = 0;
                $exp_creditTotal = 0;
            }
            $currentExp = $masterdata['account'];
            if ($amt < 0) {
                $debitTotal+=abs($amt);
                $exp_debitTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['account'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['payee'] . '</td><td class="blue_bg">' . $masterdata['exp_head'] . '</td><td class="green_bg">-</td><td class="green_bg">' . "$ " . number_format(abs($amt), 2) . '</td><td class="">' . "$ " . number_format($diff2, 2) . '</td></tr>';
                $table_exp.='<tr class="border_bottom"><td></td><td>' . $masterdata['ref'] . '</td><td colspan="6" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';
            } else {
                $creditTotal+=abs($amt);
                $exp_creditTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['account'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['payee'] . '</td><td class="blue_bg">' . $masterdata['exp_head'] . '</td><td class="green_bg">' . "$ " . number_format(abs($amt), 2) . '</td><td class="green_bg">-</td><td class="">' . "$ " . number_format($diff2, 2) . '</td></tr>';
                $table_exp.='<tr class="border_bottom"><td></td><td>' . $masterdata['ref'] . '</td><td colspan="6" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';
            }
        }

        if (abs($debitTotal) > 0) {
            $debitTotal = "$ " . number_format($debitTotal, 2);
        }
        if (abs($creditTotal) > 0) {
            $creditTotal = "$ " . number_format($creditTotal, 2);
        }
        $table_exp .= '</table><div class="page_break"></div>';
        $table_exp .= '<table cellpadding="5" cellmargin="5" style="width:100%;border:1px solid black;">';
        $table_exp.='<tr><td colspan="5">&nbsp;</td><td><hr/></td><td><hr/></td><td><hr/></td></tr>';
        $table_exp.='<tr>';
        $table_exp.='<td colspan="5"> </td>';
        $table_exp.='<td>Total</td>';
        $table_exp.='<td>' . $debitTotal . '</td>';
        $table_exp.='<td>' . $creditTotal . '</td>';
//        $table_exp.='<td>' . ($debitTotal - $creditTotal) . '</td>';
        $table_exp.='</tr>';
        $table_exp.='<tr><td colspan="5"></td><td><hr/></td><td><hr/></td><td><hr/></td></tr>';
        $table_exp.='</table>';
    } else {
        header("Location:detailed_table.php?fail");
    }

    $html = '<html>
                <head>
                    <style>
                        @page {
                            margin-header: 10mm;
                            margin-footer: 10mm;
                            margin:15px 15px 50px 15px;
                            
                            footer: MyFooter1;
                        }
                        body {font-family: sans-serif;
                              font-size: 10pt;
                        }
                        p {	margin: 0pt; }
                        .page_break{ page-break-before: always; }
                        .border_bottom td{ border-bottom:1px solid black;  }
                        .green_bg { background-color:#C8F7C5!important; }
                        .blue_bg { background-color:#4ECDC4!important; }
                    </style>
                </head>
                <body style="background-color: #eaeaea;">
                    <!--mpdf
                        <htmlpagefooter name="MyFooter1">
                            <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; margin-top:3mm;" >
                                Page {PAGENO} of {nb}
                            </div>
                        </htmlpagefooter>
                        <sethtmlpagefooter name="MyFooter1" value="on" />
                    mpdf-->
                    <div style="width: 100%; margin: 0; padding: 0;">
                        <div style="background-color:#fff;width:100%;">
                            ' . $table_exp . '
                        </div>
                    </div>
                </body>
            </html>';


    define('_MPDF_PATH', './mpdf60/');
    include("./mpdf60/mpdf.php");

    $mpdf = new mPDF('d', 'A4', '', '', 0, 0, 0, 0, 5, 5);
    $mpdf->SetProtection(array('print'));
    $mpdf->SetTitle($file_name . " " . date('Y-m-d'));
    $mpdf->SetAuthor("Vimal Patel");


    $mpdf->WriteHTML($html);
    $pdffile = $file_name . ".pdf";
//    $mpdf->Output();

    $mpdf->Output(PDF_PATH . $pdffile);
//    header("Location:" . PDF_PATH . $pdffile);
    user_log($_SESSION['user_id'], 'printed the detailed report (pdf)');
    echo PDF_PATH . $pdffile;
    exit();
} elseif (isset($action) && $action == "export_detailed_data_excel") {

    require 'vendor/autoload.php';

    $spreadsheet = new Spreadsheet();  /* ----Spreadsheet object----- */
    $Excel_writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);  /* ----- Excel (Xls) Object */
    $spreadsheet->setActiveSheetIndex(0);
    $activeSheet = $spreadsheet->getActiveSheet();
    $activeSheet->setTitle("Summary");


    /* GET SUMMARY DATA :: START */
    $getmasterreports = mysqli_query($conn, "SELECT exp_head,amount,account FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "'");
    $creditArr = $creditArr2 = [];  //  If it is negative, it is identified as the credit balance
    $debitArr = $debitArr2 = [];   //  If it is positive, it is identified as the debit balance
    $debitTotal = 0;
    $creditTotal = 0;

    if (mysqli_num_rows($getmasterreports) > 0) {
        $file_name = $division . $year;
        while ($masterdata = mysqli_fetch_assoc($getmasterreports)) {
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($amt < 0) {
                $debitArr[$masterdata['exp_head']][] = abs($amt);
                $creditArr2[$masterdata['account']][] = abs($amt);
//                $creditTotal+=abs($amt);
            } else {
                $creditArr[$masterdata['exp_head']][] = abs($amt);
                $debitArr2[$masterdata['account']][] = abs($amt);
//                $debitTotal+=abs($amt);
            }
        }
    } else {
        echo 'detailed_table.php?fail';
        exit;
    }
    $getexpheads = mysqli_query($conn, "SELECT DISTINCT exp_head FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `account`!=''  AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY exp_head, date, amount");
    $activeSheet->setCellValue('A1', 'S No')->getStyle('A1')->getFont()->setBold(true);
    $activeSheet->setCellValue('B1', 'Exp Head')->getStyle('B1')->getFont()->setBold(true);
    $activeSheet->setCellValue('C1', 'Debit')->getStyle('C1')->getFont()->setBold(true);
    $activeSheet->setCellValue('D1', 'Credit')->getStyle('D1')->getFont()->setBold(true);
    $i = 1;
    $row = 2;
    while ($exp_heads = mysqli_fetch_assoc($getexpheads)) {
        $debitAmt = (isset($debitArr[$exp_heads['exp_head']]) && count($debitArr[$exp_heads['exp_head']]) > 0) ? array_sum($debitArr[$exp_heads['exp_head']]) : "-";
        $creditAmt = (isset($creditArr[$exp_heads['exp_head']]) && count($creditArr[$exp_heads['exp_head']]) > 0) ? array_sum($creditArr[$exp_heads['exp_head']]) : "-";
        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($debitAmt - $creditAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $debitAmt = '-';
                $creditAmt = abs($diff);
            } else {
                $creditAmt = '-';
                $debitAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);

        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $activeSheet->setCellValue('A' . $row, $i);
        $activeSheet->setCellValue('B' . $row, $exp_heads['exp_head']);
        $activeSheet->setCellValue('C' . $row, $debitAmt);
        $activeSheet->setCellValue('D' . $row, $creditAmt);
        $row++;
        $i++;
    }

    $getaccount = mysqli_query($conn, "SELECT DISTINCT account FROM `reports` WHERE `status` = 1 AND `account`!='' AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY account");
    while ($accounts = mysqli_fetch_assoc($getaccount)) {
        $creditAmt = (isset($debitArr2[$accounts['account']]) && count($debitArr2[$accounts['account']]) > 0) ? array_sum($debitArr2[$accounts['account']]) : "-";
        $debitAmt = (isset($creditArr2[$accounts['account']]) && count($creditArr2[$accounts['account']]) > 0) ? array_sum($creditArr2[$accounts['account']]) : "-";

        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($creditAmt - $debitAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $creditAmt = '-';
                $debitAmt = abs($diff);
            } else {
                $debitAmt = '-';
                $creditAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);
        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $activeSheet->setCellValue('A' . $row, $i);
        $activeSheet->setCellValue('B' . $row, $accounts['account']);
        $activeSheet->setCellValue('C' . $row, $debitAmt);
        $activeSheet->setCellValue('D' . $row, $creditAmt);
        $row++;
        $i++;
    }
    if (abs($debitTotal) > 0) {
        $debitTotal = "$ " . number_format($debitTotal, 2);
    }
    if (abs($creditTotal) > 0) {
        $creditTotal = "$ " . number_format($creditTotal, 2);
    }

    $activeSheet->setCellValue('A' . $row, '');
    $activeSheet->setCellValue('B' . $row, 'Total');
    $activeSheet->setCellValue('C' . $row, $debitTotal)->getStyle('C' . $row)->getFont()->setBold(true);
    $activeSheet->setCellValue('D' . $row, $creditTotal)->getStyle('D' . $row)->getFont()->setBold(true);
    $row++;
    /* GET SUMMARY DATA :: END */

    $spreadsheet->createSheet(1);
    $spreadsheet->setActiveSheetIndex(1);
    $activeSheet = $spreadsheet->getActiveSheet();
    $activeSheet->setTitle("Detailed Report");
    $row = 1;

    $getmasterreports = mysqli_query($conn, "SELECT * FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `account`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY exp_head, date, amount");
    $creditArr = $creditArr2 = [];  //  If it is negative, it is identified as the credit balance
    $debitArr = $debitArr2 = [];   //  If it is positive, it is identified as the debit balance
    $debitTotal = 0;
    $creditTotal = 0;
    $exp_creditTotal = 0;
    $exp_debitTotal = 0;
//    $reportsArr = [];

    if (mysqli_num_rows($getmasterreports) > 0) {
        $file_name = $division . $year;

        $activeSheet->setCellValue('A1', 'Exp Head')->getStyle('A1')->getFont()->setBold(true);
        $activeSheet->setCellValue('B1', 'Date')->getStyle('B1')->getFont()->setBold(true);
        $activeSheet->setCellValue('C1', 'Ref')->getStyle('C1')->getFont()->setBold(true);
        $activeSheet->setCellValue('D1', 'Payee')->getStyle('D1')->getFont()->setBold(true);
        $activeSheet->setCellValue('E1', 'Account')->getStyle('E1')->getFont()->setBold(true);
        $activeSheet->setCellValue('F1', 'Memo')->getStyle('F1')->getFont()->setBold(true);
        $activeSheet->setCellValue('G1', 'Debit')->getStyle('G1')->getFont()->setBold(true);
        $activeSheet->setCellValue('H1', 'Credit')->getStyle('H1')->getFont()->setBold(true);
        $activeSheet->setCellValue('I1', 'Balance')->getStyle('I1')->getFont()->setBold(true);
        $row = 2;

        $currentExp = '';
        while ($masterdata = mysqli_fetch_assoc($getmasterreports)) {
//            $reportsArr[] = $masterdata;
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($currentExp != $masterdata['exp_head'] && $currentExp != '') {
                $row++;
                $exp_debitTotal = 0;
                $exp_creditTotal = 0;
            }
            $currentExp = $masterdata['exp_head'];
            if ($amt < 0) {
                $creditTotal+=abs($amt);
                $exp_creditTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
//                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['exp_head'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['ref'] . '</td><td>' . $masterdata['payee'] . '</td><td>' . $masterdata['account'] . '</td><td class="green_bg">-</td><td class="green_bg">' . "$ " . number_format(abs($amt), 2) . '</td><td class="green_bg">' . "$ " . number_format($diff2, 2) . '</td></tr>';
//                $table_exp.='<tr class="border_bottom"><td></td><td></td><td colspan="6" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';
                $activeSheet->setCellValue('A' . $row, $masterdata['exp_head']);
                $activeSheet->setCellValue('B' . $row, $masterdata['date']);
                $activeSheet->setCellValue('C' . $row, $masterdata['ref']);
                $activeSheet->setCellValue('D' . $row, $masterdata['payee']);
                $activeSheet->setCellValue('E' . $row, $masterdata['account']);
                $activeSheet->setCellValue('F' . $row, $masterdata['memo']);
                $activeSheet->setCellValue('G' . $row, '-');
                $activeSheet->setCellValue('H' . $row, "$ " . number_format(abs($amt), 2));
                $activeSheet->setCellValue('I' . $row, "$ " . number_format($diff2, 2));
                $row++;
            } else {
                $debitTotal+=abs($amt);
                $exp_debitTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
//                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['exp_head'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['ref'] . '</td><td>' . $masterdata['payee'] . '</td><td>' . $masterdata['account'] . '</td><td class="green_bg">' . "$ " . number_format(abs($amt), 2) . '</td><td class="green_bg">-</td><td class="green_bg">' . "$ " . number_format($diff2, 2) . '</td></tr>';
//                $table_exp.='<tr class="border_bottom"><td></td><td></td><td colspan="6" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';
                $activeSheet->setCellValue('A' . $row, $masterdata['exp_head']);
                $activeSheet->setCellValue('B' . $row, $masterdata['date']);
                $activeSheet->setCellValue('C' . $row, $masterdata['ref']);
                $activeSheet->setCellValue('D' . $row, $masterdata['payee']);
                $activeSheet->setCellValue('E' . $row, $masterdata['account']);
                $activeSheet->setCellValue('F' . $row, $masterdata['memo']);
                $activeSheet->setCellValue('G' . $row, "$ " . number_format(abs($amt), 2));
                $activeSheet->setCellValue('H' . $row, '-');
                $activeSheet->setCellValue('I' . $row, "$ " . number_format($diff2, 2));
                $row++;
            }
        }
        $row+=2;
        $activeSheet->setCellValue('A' . $row, 'Account')->getStyle('A' . $row)->getFont()->setBold(true);
        $activeSheet->setCellValue('B' . $row, 'Date')->getStyle('B' . $row)->getFont()->setBold(true);
        $activeSheet->setCellValue('C' . $row, 'Ref')->getStyle('C' . $row)->getFont()->setBold(true);
        $activeSheet->setCellValue('D' . $row, 'Payee')->getStyle('D' . $row)->getFont()->setBold(true);
        $activeSheet->setCellValue('E' . $row, 'Exp Head')->getStyle('E' . $row)->getFont()->setBold(true);
        $activeSheet->setCellValue('F' . $row, 'Memo')->getStyle('F' . $row)->getFont()->setBold(true);
        $activeSheet->setCellValue('G' . $row, 'Debit')->getStyle('G' . $row)->getFont()->setBold(true);
        $activeSheet->setCellValue('H' . $row, 'Credit')->getStyle('H' . $row)->getFont()->setBold(true);
        $activeSheet->setCellValue('I' . $row, 'Balance')->getStyle('I' . $row)->getFont()->setBold(true);
        $row++;

        $getmasterreports1 = mysqli_query($conn, "SELECT * FROM `reports` WHERE `status` = 1 AND `account`!='' AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY account, date, amount");
        $exp_creditTotal = 0;
        $exp_debitTotal = 0;
        $currentExp = '';
        while ($masterdata = mysqli_fetch_assoc($getmasterreports1)) {
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($currentExp != $masterdata['account'] && $currentExp != '') {
                $row++;
                $exp_debitTotal = 0;
                $exp_creditTotal = 0;
            }
            $currentExp = $masterdata['account'];
            if ($amt < 0) {
                $debitTotal+=abs($amt);
                $exp_debitTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
//                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['account'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['ref'] . '</td><td>' . $masterdata['payee'] . '</td><td>' . $masterdata['exp_head'] . '</td><td class="green_bg">-</td><td class="green_bg">' . "$ " . number_format(abs($amt), 2) . '</td><td class="green_bg">' . "$ " . number_format($diff2, 2) . '</td></tr>';
//                $table_exp.='<tr class="border_bottom"><td></td><td></td><td colspan="6" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';
                $activeSheet->setCellValue('A' . $row, $masterdata['account']);
                $activeSheet->setCellValue('B' . $row, $masterdata['date']);
                $activeSheet->setCellValue('C' . $row, $masterdata['ref']);
                $activeSheet->setCellValue('D' . $row, $masterdata['payee']);
                $activeSheet->setCellValue('E' . $row, $masterdata['exp_head']);
                $activeSheet->setCellValue('F' . $row, $masterdata['memo']);
                $activeSheet->setCellValue('G' . $row, '-');
                $activeSheet->setCellValue('H' . $row, "$ " . number_format(abs($amt), 2));
                $activeSheet->setCellValue('I' . $row, "$ " . number_format($diff2, 2));
                $row++;
            } else {
                $creditTotal+=abs($amt);
                $exp_creditTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
//                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['account'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['ref'] . '</td><td>' . $masterdata['payee'] . '</td><td>' . $masterdata['exp_head'] . '</td><td class="green_bg">' . "$ " . number_format(abs($amt), 2) . '</td><td class="green_bg">-</td><td class="green_bg">' . "$ " . number_format($diff2, 2) . '</td></tr>';
//                $table_exp.='<tr class="border_bottom"><td></td><td></td><td colspan="6" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';

                $activeSheet->setCellValue('A' . $row, $masterdata['account']);
                $activeSheet->setCellValue('B' . $row, $masterdata['date']);
                $activeSheet->setCellValue('C' . $row, $masterdata['ref']);
                $activeSheet->setCellValue('D' . $row, $masterdata['payee']);
                $activeSheet->setCellValue('E' . $row, $masterdata['exp_head']);
                $activeSheet->setCellValue('F' . $row, $masterdata['memo']);
                $activeSheet->setCellValue('G' . $row, "$ " . number_format(abs($amt), 2));
                $activeSheet->setCellValue('H' . $row, '-');
                $activeSheet->setCellValue('I' . $row, "$ " . number_format($diff2, 2));
                $row++;
            }
        }

        if (abs($debitTotal) > 0) {
            $debitTotal = "$ " . number_format($debitTotal, 2);
        }
        if (abs($creditTotal) > 0) {
            $creditTotal = "$ " . number_format($creditTotal, 2);
        }

        $activeSheet->setCellValue('F' . $row, 'Total')->getStyle('F' . $row)->getFont()->setBold(true);
        $activeSheet->setCellValue('G' . $row, $debitTotal)->getStyle('G' . $row)->getFont()->setBold(true);
        $activeSheet->setCellValue('H' . $row, $creditTotal)->getStyle('H' . $row)->getFont()->setBold(true);
    } else {
        header("Location:detailed_table.php?fail");
    }


    $filename = 'Detailed report for ' . $file_name;
    $Excel_writer->save('uploads/' . $filename . '.xlsx');
    user_log($_SESSION['user_id'], 'printed the detailed report (excel)');
    echo 'uploads/' . $filename . '.xlsx';
    exit();
} elseif (isset($action) && $action == "show_master_report") {
    $division_txt = "";
    if ($division != '') {
        $division_txt = " AND division = '" . $division . "'";


        $res_data = mysqli_query($conn, "SELECT * FROM `master_reports` WHERE `status`=1 $division_txt ORDER BY master_reports_id DESC");

        $i = 1;
        $html = '';
        if (mysqli_num_rows($res_data) > 0) {
            while ($data_row = mysqli_fetch_assoc($res_data)) {
                $html .= '<tr id="record_' . $data_row['master_reports_id'] . '">
                        <td>' . $i++ . '</td>
                        <td>' . $data_row['payee'] . '</td>
                        <td>' . $data_row['exp_head'] . '</td>
                        <td>' . $data_row['division'] . '</td>
                        <td>
                            <a href="master_reports_manage.php?master_reports_id=' . base64_encode($data_row['master_reports_id']) . '" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>';
            }
        } else {
            $html .= '<tr>
                    <td class="display-none"><td class="display-none"><td class="display-none"><td class="display-none">
                    <td colspan="5" class="text-center">No Record Found</td>
                </tr>';
        }
        $table = '<table class="table table-hover show-master-report">
                    <thead>
                        <th>ID</th>
                        <th>Payee</th>
                        <th>Exp Head</th>
                        <th>Division</th>
                        <th>Action</th>
                    </thead>
                    <tbody>' . $html . ' </tbody>
                </table>';
        echo $table;
    } else {
        $table = '<table class="table table-hover show-master-report">
                    <thead>
                        <th>ID</th>
                        <th>Payee</th>
                        <th>Exp Head</th>
                        <th>Division</th>
                        <th>Action</th>
                    </thead>
                    <tbody></tbody>
                </table>';
        echo $table;
    }
} elseif (isset($action) && $action == "show_report_listing") {
    $division_txt = $year_txt = "";
    if ($division != '') {
        $division_txt = " AND division = '" . $division . "'";

        if ($year != '') {
            $year_txt = " AND year = '" . $year . "'";
        }
        $res_data = mysqli_query($conn, "SELECT * FROM `reports` WHERE `status`=1 $division_txt $year_txt ORDER BY reports_id DESC");

        $i = 1;
        $html = '';
        if (mysqli_num_rows($res_data) > 0) {
            while ($data_row = mysqli_fetch_assoc($res_data)) {
                $html .= '<tr id="record_' . $data_row['reports_id'] . '">
                        <td>' . $i++ . '</td>
                        <td>' . $data_row['date'] . '</td>
                        <td>' . $data_row['ref'] . '</td>
                        <td>' . $data_row['account'] . '</td>
                        <td>' . $data_row['payee'] . '</td>
                        <td>' . $data_row['memo'] . '</td>
                        <td>' . $data_row['amount'] . '</td>
                        <td>' . $data_row['exp_head'] . '</td>
                        <td>' . $data_row['division'] . '</td>
                        <td>' . $data_row['year'] . '</td>
                        <td>
                            <a href="reports_manage.php?reports_id=' . base64_encode($data_row['reports_id']) . '" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>';
            }
        } else {
            $html .= '<tr>
                    <td class="display-none"></td><td class="display-none"></td><td class="display-none"></td><td class="display-none"></td>
                    <td class="display-none"></td><td class="display-none"></td><td class="display-none"></td><td class="display-none"></td>
                    <td class="display-none"></td><td class="display-none"></td>
                    <td colspan="11" class="text-center">No Record Found</td>
                </tr>';
        }
        $table = '<table class="table table-hover show-report-listing">
                    <thead>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Ref</th>
                    <th>Account</th>
                    <th>Payee</th>
                    <th>Memo</th>
                    <th>Amount</th>
                    <th>Exp Head</th>
                    <th>Division</th>
                    <th>Year</th>
                    <th>Action</th>
                    </thead>
                    <tbody>' . $html . '</tbody>
                </table>';
        echo $table;
    } else {
        $table = '<table class="table table-hover show-report-listing">
                    <thead>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Ref</th>
                    <th>Account</th>
                    <th>Payee</th>
                    <th>Memo</th>
                    <th>Amount</th>
                    <th>Exp Head</th>
                    <th>Division</th>
                    <th>Year</th>
                    <th>Action</th>
                    </thead>
                    <tbody></tbody>
                </table>';
        echo $table;
    }
} elseif (isset($action) && $action == "show_process_list") {
    $division_txt = $year_txt = "";
    if ($division != '') {
        $division_txt = " AND division = '" . $division . "'";

        if ($year != '') {
            $year_txt = " AND year = '" . $year . "'";
        }
        $res_data = mysqli_query($conn, "SELECT * FROM `reports` WHERE `status`=1 AND `exp_head` = '' $division_txt $year_txt ORDER BY reports_id DESC");

        $i = 1;
        $html = '';
        if (mysqli_num_rows($res_data) > 0) {
            while ($data_row = mysqli_fetch_assoc($res_data)) {
                $html .= '<tr id="record_' . $data_row['reports_id'] . '">
                        <td>' . $i++ . '</td>
                        <td>' . $data_row['date'] . '</td>
                        <td>' . $data_row['ref'] . '</td>
                        <td>' . $data_row['account'] . '</td>
                        <td>' . $data_row['payee'] . '</td>
                        <td>' . $data_row['memo'] . '</td>
                        <td>' . $data_row['amount'] . '</td>
                        <td>' . $data_row['exp_head'] . '</td>
                        <td>
                            <a href="reports_manage.php?reports_id=' . base64_encode($data_row['reports_id']) . '" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>';
            }
            $start_process_btn = '<form action="functions.php" method="POST">
                                <input type="hidden" name="action" value="process_expence_data">
                                <button type="submit" class="btn btn-info btn-fill pull-right">Start Process</button>
                                <div class="clearfix"></div>
                            </form>';
        } else {
            $html .= '<tr>
                    <td class="display-none"></td><td class="display-none"></td><td class="display-none"></td><td class="display-none"></td>
                    <td class="display-none"></td><td class="display-none"></td><td class="display-none"></td><td class="display-none"></td>
                    <td colspan="9" class="text-center">No Record Found</td>
                </tr>';
            $start_process_btn = '';
        }
        $table = '<table class="table table-hover show-report-listing">
                <thead>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Ref</th>
                    <th>Account</th>
                    <th>Payee</th>
                    <th>Memo</th>
                    <th>Amount</th>
                    <th>Exp Head</th>
                    <th>Action</th>
                </thead>
                <tbody> ' . $html . ' </tbody>
                </table>
                ' . $start_process_btn;
        echo $table;
    } else {
        $table = '<table class="table table-hover show-report-listing">
                <thead>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Ref</th>
                    <th>Account</th>
                    <th>Payee</th>
                    <th>Memo</th>
                    <th>Amount</th>
                    <th>Exp Head</th>
                    <th>Action</th>
                </thead>
                <tbody> </tbody>
                </table>';
        echo $table;
    }
} elseif (isset($action) && $action == "add_division") {
    $current_date = date('Y-m-d H:i:s');
    $insert = "INSERT INTO division (division, status, added_on, modified_on) VALUES('" . mysqli_real_escape_string($conn, $division) . "',1,'" . $current_date . "','" . $current_date . "')";
    $result = mysqli_query($conn, $insert);
    if ($result) {
        user_log($_SESSION['user_id'], 'added new division "' . $division . '"');
        header("Location:division_manage.php");
    }
} elseif (isset($action) && $action == "add_year") {
    $current_date = date('Y-m-d H:i:s');
    $insert = "INSERT INTO year (division_id,year, status, added_on, modified_on) VALUES('" . mysqli_real_escape_string($conn, $division_id) . "','" . mysqli_real_escape_string($conn, $year) . "',1,'" . $current_date . "','" . $current_date . "')";
    $result = mysqli_query($conn, $insert);
    if ($result) {
        user_log($_SESSION['user_id'], 'added new period "' . $year . '"');
        header("Location:period_manage.php?did=" . base64_encode($division_id));
    }
} elseif (isset($action) && $action == "delete_division") {
    $current_date = date('Y-m-d H:i:s');
    $division_id = $_POST['division_id'];
    $update = "UPDATE `division` SET `status`=0 WHERE division_id = '" . mysqli_real_escape_string($conn, $division_id) . "'";
    $result = mysqli_query($conn, $update);
    if ($result) {
        //TODO :: need to repove reports data for this division
        user_log($_SESSION['user_id'], 'deleted division ');
        echo 'division_manage.php?success';
    } else {
        echo 'division_manage.php?fail';
    }
} elseif (isset($action) && $action == "delete_year") {
    $current_date = date('Y-m-d H:i:s');
    $year_id = $_POST['year_id'];
    $update = "UPDATE `year` SET `status`=0 WHERE year_id = '" . mysqli_real_escape_string($conn, $year_id) . "'";
    $result = mysqli_query($conn, $update);
    if ($result) {
        //TODO :: need to repove reports data for this division
        user_log($_SESSION['user_id'], 'deleted Period ');
        echo ("period_manage.php?did=" . base64_encode($division_id) . "&success");
    } else {
        echo ("period_manage.php?did=" . base64_encode($division_id) . "&fail");
    }
} elseif (isset($action) && $action == "fill_year") {
    $getyear = mysqli_query($conn, "SELECT y.* FROM `year` AS y WHERE y.division_id = ( SELECT d.division_id FROM division AS d WHERE d.division = '" . $division . "' AND d.`status` = 1 );");
    $html = '<option value="">Select Year</option>';
    while ($years = mysqli_fetch_assoc($getyear)) {
        $html.='<option value="' . $years['year'] . '">' . $years['year'] . '</option>';
    }
    echo $html;
}