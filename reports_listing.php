<?php
ob_start();

include_once './inner_header.php';
include_once './db_connection.php';

global $conn;
$permission = [];
if ($_SESSION['is_admin'] != 1) {
    $permission = get_permissions($_SESSION['user_id'], 'upload_permission');
}
is_user_active();
set_user_active_time();

$basic_data = [];
$alldivisions = get_divisions();
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header row">
                        <form method="POST" id="report_listing">
                            <input type="hidden" name="action" value="show_report_listing">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Division</label>
                                    <select class="form-control" name="division" onchange="fill_year(this.value)" required="">
                                        <option value="">Select Division</option>
                                        <?php
                                        if (count($alldivisions) > 0) {
                                            foreach ($alldivisions as $key => $dv) {
                                                $disabled = '';
                                                if (!key_exists($dv, $permission) && $_SESSION['is_admin'] != 1) {
                                                    $disabled = 'disabled';
                                                }
                                                echo '<option value="' . $dv . '" ' . $disabled . '>' . $dv . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <div class="form-group">
                                    <label>Year</label>
                                    <select class="form-control" name="year" id="year_dropdown" required=""></select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-flat btn-primary btn-sm m-t-25" onclick="show_report_listing();">Show Report</a>
                            </div>
                        </form>
                    </div>
                    <div class="header row">
                        <?php if (isset($_GET['n']) && $_GET['n'] != '') { ?>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <p class="alert alert-info">
                                    <?php echo base64_decode($_GET['n']) . " record(s) are affected"; ?>
                                </p>
                            </div>
                        <?php }
                        ?>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <h4 class="title">List of Expense Reports</h4>
                            <p class="category">Edit Data from Action</p>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <a href="reports_manage.php" class="btn btn-flat btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Add New</a>
                        </div>

                    </div>
                    <div class="content table-responsive show-report"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once './inner_footer.php';
