<?php
ob_start();

include_once './inner_header.php';
include_once './db_connection.php';

//if ($_SESSION['admin_user_type'] != 1) {
//    header("Location: admin_track_sheet_data_listing.php");
//}
$master_reports_id = '-1';
//Check if Category is open in Edit mode or Insert mode
if (isset($_GET['master_reports_id']) && $_GET['master_reports_id'] != '') {
    // get data as its update form
    $master_reports_id = $_GET['master_reports_id'] = base64_decode($_GET['master_reports_id']);
    $title = "Edit Report";
    $action = "edit_master_report";
    $basic_data = [];
    $res_data = mysqli_query($conn, "SELECT * FROM `master_reports` WHERE master_reports_id = '" . $master_reports_id . "'");
    $res = mysqli_fetch_assoc($res_data);
} else {
    $title = "Add New Report";
    $action = "add_master_report";
}
$permission = [];
if ($_SESSION['is_admin'] != 1) {
    $permission = get_permissions($_SESSION['user_id'], 'report_permission');
}
$alldivisions = get_divisions();
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title"><?= $title ?></h4>
                    </div>
                    <div class="content">
                        <form class="form-horizontal" action="functions.php" method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="action" value="<?php echo $action; ?>">
                            <input type="hidden" name="master_reports_id" id="master_reports_id" value="<?php echo $master_reports_id; ?>">
                            <div class="">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Payee</label>
                                        <input type="text" class="form-control" id="payee" name="payee" placeholder="Enter payee" autocomplete="off" value="<?php echo (isset($res['payee']) && $res['payee'] != '') ? $res['payee'] : ''; ?>" required=""/>
                                    </div>
                                </div>
                                <div class="col-md-3 col-md-offset-1">
                                    <div class="form-group">
                                        <label>Exp head</label>
                                        <input type="text" class="form-control" id="exp_head" name="exp_head" placeholder="Enter Exp head" autocomplete="off" value="<?php echo (isset($res['exp_head']) && $res['exp_head'] != '') ? $res['exp_head'] : ''; ?>" required=""/>
                                    </div>
                                </div>
                                <div class="col-md-3 col-md-offset-1">
                                    <div class="form-group">
                                        <label>Division</label>
                                        <?php
                                        if ($action == 'edit_master_report') {
                                            ?>
                                            <span class="form-control"> <?php echo $res['division']; ?> </span>
                                            <?php
                                        } else {
                                            ?>
                                            <select class="form-control" name="division">
                                                <?php
                                                if (count($alldivisions) > 0) {
                                                    foreach ($alldivisions as $key => $dv) {
                                                        $disabled = '';
                                                        if (!key_exists($dv, $permission) && $_SESSION['is_admin'] != 1) {
                                                            $disabled = 'disabled';
                                                        }
                                                        echo '<option value="' . $dv . '" ' . $disabled . '>' . $dv . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <?php
                                        }
                                        ?>


                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <button type="submit" class="btn btn-info btn-fill ">Save</button>
                                <a href="master_reports_listing.php" class="btn btn-default ">Cancel</a>
                            </div>

                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once './footer.php';
?>