<?php
include_once './header.php';
?>
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-form-title" style="background-image: url(images/bg-01.jpg);">
                <span class="login100-form-title-1">
                    Register Yourself
                </span>
            </div>

            <form role="form" class="login100-form validate-form" id="register_form" action="javascript:void(0)" method="post" onsubmit="register_user();">
                <input type="hidden" name="action" value="register_user">
                <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                    <span class="label-input100">Username</span>
                    <input class="input100" type="text" name="username" id="username" placeholder="Enter username" autocomplete="off" value="">
                    <span class="focus-input100"></span>
                </div>
                <div class="wrap-input100 validate-input m-b-26" data-validate="Email id is required">
                    <span class="label-input100">Email id</span>
                    <input class="input100" type="email" name="email_id" id="email_id" placeholder="Enter Email ID" autocomplete="off" value="">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                    <span class="label-input100">Password</span>
                    <input class="input100" type="password" name="password" id="password" placeholder="Enter password" autocomplete="off" value="">
                    <span class="focus-input100"></span>
                </div>

                <div class="flex-sb-m w-full p-b-30">
                    <div class="contact100-form-checkbox">
                    </div>

                    <div>
                        <a href="index.php" class="txt1">
                            Sign in
                        </a>
                    </div>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Register
                    </button>
                </div>
                
            </form>
            <div class="alert_message" style="display: none;"></div>
        </div>
    </div>
</div>
<?php
include_once './footer.php';
?>
