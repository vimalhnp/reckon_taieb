<?php
ob_start();
//session_start();
include_once './inner_header.php';
include_once './db_connection.php';

global $conn;

if ($_SESSION['is_admin'] != 1) {
    header("Location:master_reports_listing.php");
}
is_user_active();
set_user_active_time();

$basic_data = [];
$res_data = mysqli_query($conn, "SELECT * FROM `user` WHERE is_admin != 1 AND `status`=1 ORDER BY user_id DESC");

$divisionArr = get_divisions();
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <h4 class="title">List of Users</h4>
                            <p class="category">Assign Permissions to Users</p>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <a href="user_addedit.php" class="btn btn-flat btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Add New</a>
                        </div>

                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover ">
                            <thead>
                            <th>ID</th>
                            <th>Email</th>
                            <!--<th>Division</th>-->
                            <th>Permission</th>
                            <!--<th>Active?</th>-->
                            <th>Action</th>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                if (mysqli_num_rows($res_data) > 0) {
                                    while ($data_row = mysqli_fetch_assoc($res_data)) {
                                        $upload_permission = $process_permission = $report_permission = [];
                                        if (isset($data_row['upload_permission']) && $data_row['upload_permission'] != '') {
                                            $upload_permission = json_decode($data_row['upload_permission'], TRUE);
                                        }
                                        if (isset($data_row['process_permission']) && $data_row['process_permission'] != '') {
                                            $process_permission = json_decode($data_row['process_permission'], TRUE);
                                        }
                                        if (isset($data_row['report_permission']) && $data_row['report_permission'] != '') {
                                            $report_permission = json_decode($data_row['report_permission'], TRUE);
                                        }
                                        ?>
                                        <tr id="record_<?php echo $data_row['user_id']; ?>">
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo $data_row['email_id']; ?></td>
                                            <!--<td><?php echo $data_row['division']; ?></td>-->
                                            <td>
                                                <form  id="form_<?php echo $data_row['user_id']; ?>" action="#">
                                                    <table class="table table-responsive table-bordered">
                                                        <tr>
                                                            <th>Division</th>
                                                            <th>Upload</th>
                                                            <th>Process</th>
                                                            <th>Report</th>
                                                        </tr>
                                                        <?php
                                                        if (count($divisionArr) > 0) {

                                                            foreach ($divisionArr as $div_val) {
                                                                echo '<tr>';
                                                                echo '<td>' . $div_val . '</td>';
                                                                $uploadChecked = '';
                                                                $processChecked = '';
                                                                $reportChecked = '';
                                                                if (isset($upload_permission[$div_val]) && $upload_permission[$div_val][0] == 1) {
                                                                    $uploadChecked = 'checked';
                                                                }
                                                                if (isset($process_permission[$div_val]) && $process_permission[$div_val][0] == 1) {
                                                                    $processChecked = 'checked';
                                                                }
                                                                if (isset($report_permission[$div_val]) && $report_permission[$div_val][0] == 1) {
                                                                    $reportChecked = 'checked';
                                                                }
                                                                echo '<td><label class="switch">
                                                                    <input type="checkbox" name="upload_permission[' . $div_val . '][]" onchange="change_permission_new(' . $data_row['user_id'] . ');" value="1" ' . $uploadChecked . '>
                                                                        <span class="slider round"></span>
                                                                    </label></td>';
                                                                echo '<td><label class="switch">
                                                                    <input type="checkbox" name="process_permission[' . $div_val . '][]" onchange="change_permission_new(' . $data_row['user_id'] . ');" value="1" ' . $processChecked . '>
                                                                        <span class="slider round"></span>
                                                                    </label></td>';
                                                                echo '<td><label class="switch">
                                                                    <input type="checkbox" name="report_permission[' . $div_val . '][]" onchange="change_permission_new(' . $data_row['user_id'] . ');" value="1" ' . $reportChecked . '>
                                                                        <span class="slider round"></span>
                                                                    </label></td>';
                                                                echo '</tr>';
                                                            }
                                                        }
                                                        ?>
                                                    </table>
                                                </form>
                                            </td>
<!--                                            <td>
                                                <label class="switch">
                                                    <input type="checkbox" name="status" onchange="change_permission('status', this.checked,<?php echo $data_row['user_id']; ?>);" <?php echo ($data_row['status'] == 1) ? 'checked' : ''; ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>-->
                                            <td>
                                                <a href="user_addedit.php?user_id=<?php echo base64_encode($data_row['user_id']); ?>" title="UPDATE PASSWORD" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                                                <a href="javascript:void(0);" onclick="deleteUser(<?php echo ($data_row['user_id']); ?>)" title="Delete User" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once './inner_footer.php';
