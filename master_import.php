<?php
ob_start();
//session_start();
include_once './inner_header.php';
include_once './db_connection.php';
global $conn;

$permission=[];
if ($_SESSION['is_admin'] != 1) {
    $permission = get_permissions($_SESSION['user_id'], 'upload_permission');
}
is_user_active();
set_user_active_time();
$alldivisions = get_divisions();
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Master Table</h4>
                    </div>
                    <div class="content">
                        <form action="functions.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="master_upload_file">
                            <div class="row">
                                <div class="col-md-3 col-md-offset-3">
                                    <div class="form-group">
                                        <label>Division</label>
                                        <select class="form-control" name="division" required="">
                                            <option value="">Select Division</option>
                                            <?php
                                            if (count($alldivisions) > 0) {
                                            foreach ($alldivisions as $key => $dv) {
                                                    $disabled = '';
                                                    if (!key_exists($dv, $permission) && $_SESSION['is_admin'] != 1) {
                                                        $disabled = 'disabled';
                                                    }
                                                    echo '<option value="' . $dv . '" ' . $disabled . '>' . $dv . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Import Action</label>
                                        <select class="form-control" name="import_action">
                                            <option value="append" >Append</option>
                                            <option value="replace" >Replace</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="form-group">
                                        <label>Master Report File (Excel)</label>
                                        <input type="file" class="form-control" name="report_file" placeholder="Report">
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-info btn-fill pull-right">Import Report</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once './inner_footer.php';
