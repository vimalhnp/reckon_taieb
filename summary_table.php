<?php
ob_start();
//session_start();
include_once './inner_header.php';
include_once './db_connection.php';

global $conn;
$permission = [];
if ($_SESSION['is_admin'] != 1) {
    $permission = get_permissions($_SESSION['user_id'], 'report_permission');
}
is_user_active();
set_user_active_time();

$basic_data = [];
$alldivisions = get_divisions();
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php
                if (isset($_GET['fail'])) {
                    echo '<p class="alert alert-danger col-md-12">There is no data found for this Division and Year!</p>';
                }
                ?>

                <div class="card">
                    <div class="header row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <h4 class="title">Summary Report</h4>
                            <p class="category">Export Data</p>
                        </div>
                    </div>
                    <div class="content">
                        <form class="form-horizontal" method="post" id="summary_form">
                            <input type="hidden" name="action" value="export_summary_data">
                            <div class="">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Division</label>
                                        <select class="form-control" name="division" onchange="fill_year(this.value)" required="">
                                            <option value="">Select Division</option>
                                            <?php
                                            if (count($alldivisions) > 0) {
                                                foreach ($alldivisions as $key => $dv) {
                                                    $disabled = '';
                                                    if (!key_exists($dv, $permission) && $_SESSION['is_admin'] != 1) {
                                                        $disabled = 'disabled';
                                                    }
                                                    echo '<option value="' . $dv . '" ' . $disabled . '>' . $dv . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-md-offset-1">
                                    <div class="form-group">
                                        <label>Year</label>
                                        <select class="form-control" name="year" id="year_dropdown" required=""></select>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <!--<button type="submit" class="btn btn-info btn-fill ">Export PDF</button>-->
                                <a class="btn btn-info btn-fill" onclick="export_summary_data_pdf();">Export PDF</a>
                                <button type="button" class="btn btn-info btn-fill" onclick="export_summary_data_excel();">Export Excel</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                        <!--                        <form action="functions.php" method="POST" enctype="multipart/form-data">
                                                    <input type="hidden" name="action" value="export_summary_data_excel">
                                                    <button type="submit" class="btn btn-info btn-fill pull-right">Export Excel</button>
                                                    <div class="clearfix"></div>
                                                </form>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once './inner_footer.php';
