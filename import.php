<?php
ob_start();
//session_start();
include_once './inner_header.php';
include_once './db_connection.php';
global $conn;

$permission = [];
if ($_SESSION['is_admin'] != 1) {
    $permission = get_permissions($_SESSION['user_id'], 'upload_permission');
}
is_user_active();
set_user_active_time();

$alldivisions = get_divisions();
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Expense Table</h4>
                    </div>
                    <div class="content">
                        <form action="functions.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="upload_file">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Division</label>
                                        <select class="form-control" name="division" onchange="fill_year(this.value)" required="">
                                            <option value="">Select Division</option>
                                            <?php
                                            if (count($alldivisions) > 0) {
                                                foreach ($alldivisions as $key => $dv) {
                                                    $disabled = '';
                                                    if (!key_exists($dv, $permission) && $_SESSION['is_admin'] != 1) {
                                                        $disabled = 'disabled';
                                                    }
                                                    echo '<option value="' . $dv . '" ' . $disabled . '>' . $dv . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Year  (e.g.: 2018 OR 1718)</label>
                                        <select class="form-control" name="year" id="year_dropdown" required=""></select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Import Action</label>
                                        <select class="form-control" name="import_action">
                                            <option value="append" >Append</option>
                                            <option value="replace" >Replace</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Report File (Excel)</label>
                                        <input type="file" class="form-control" name="report_file" placeholder="Report">
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-info btn-fill pull-right">Import Report</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once './inner_footer.php';
