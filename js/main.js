
(function ($) {
    "use strict";

    /*==================================================================
     [ Focus Contact2 ]*/
    $('.input100').each(function () {
        $(this).on('blur', function () {
            if ($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })
    })

    /*==================================================================
     [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit', function () {
        var check = true;

        for (var i = 0; i < input.length; i++) {
            if (validate(input[i]) == false) {
                showValidate(input[i]);
                check = false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function () {
        $(this).focus(function () {
            hideValidate(this);
        });
    });

    function validate(input) {
        if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if ($(input).val().trim() == '') {
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }


})(jQuery);
function register_user() {
    if ($("#username").val() != '' && $("#email_id").val() != '' && $("#password").val() != '') {
        $.ajax({
            url: "functions.php",
            type: "POST",
            data: $('#register_form').serialize(),
            cache: false,
            success: function (response) {
                if (response == "Successful Registration")
                {
                    $(".alert_message").html('<div class="alert alert-success"><strong>Success!</strong> Registered Successfully</div>').show();
                    $(".alert_message").fadeTo(2000, 500).slideUp(500, function () {
                        window.location.href = 'index.php';
                    });
                    return true;
                } else if (response == 'Already Username Exist') {
                    $(".alert_message").html('<div class="alert alert-danger"><strong>Oops!</strong> Username Already Used!</div>').show();
                    return false;
                }
                else
                {
                    $(".alert_message").html('<div class="alert alert-danger"><strong>Oops!</strong> Something went wrong!</div>').show();
                    return false;
                }
            }
        });
    }
}
function login_user() {
    if ($("#username").val() != '' && $("#password").val() != '') {
        $.ajax({
            url: "functions.php",
            type: "POST",
            data: $('#login_form').serialize(),
            cache: false,
            success: function (response) {
                if (response == "success")
                {
                    $(".alert_message").html('<div class="alert alert-success"><strong>Success!</strong> Logged In Successfully</div>').show();
                    $(".alert_message").fadeTo(2000, 500).slideUp(500, function () {
                        window.location.href = 'import.php';
                    });
                    return true;
                }
                else
                {
                    $(".alert_message").html('<div class="alert alert-danger"><strong>Oops!</strong> Something went wrong!</div>').show();
                    return false;
                }
            }
        });
    }
}
function logout_user() {
    $.ajax({
        url: "functions.php",
        type: "POST",
        data: {'action': 'logout_user'},
        cache: false,
        success: function (response) {
            window.location.href = 'index.php';
        }
    });
}
function change_permission(field, state, user_id) {
    $.ajax({
        url: "functions.php",
        type: "POST",
        data: {'action': 'change_permission', 'field': field, 'state': state, 'user_id': user_id},
        cache: false,
        success: function (response) {
//            window.location.href = 'index.php';
        }
    });
}
function change_permission_new(user_id) {
    var formdata = $("#form_" + user_id).serialize();
    formdata += "&action=change_permission_new&user_id=" + user_id

    $.ajax({
        url: "functions.php",
        type: "POST",
        data: formdata,
        cache: false,
        success: function (response) {
//            window.location.href = 'index.php';
        }
    });
}
function export_summary_data_excel() {
    $("#summary_form").find('input[name=action]').attr("disabled", true);
    var formdata = $("#summary_form").serialize();
    formdata += "&action=export_summary_data_excel";
    $.ajax({
        url: "functions.php",
        type: "POST",
        data: formdata,
        cache: false,
        success: function (response) {
            $("#summary_form").find('input[name=action]').removeAttr('disabled');
            window.location.href = response;
        }
    });
}
function export_detailed_data_excel() {
    $("#detailed_form").find('input[name=action]').attr("disabled", true);
    var formdata = $("#detailed_form").serialize();
    formdata += "&action=export_detailed_data_excel";
    $.ajax({
        url: "functions.php",
        type: "POST",
        data: formdata,
        cache: false,
        success: function (response) {
            $("#detailed_form").find('input[name=action]').removeAttr('disabled');
            window.location.href = response;
        }
    });
}
function show_master_report() {
    var formdata = $("#master_report").serialize();
    $.ajax({
        url: "functions.php",
        type: "POST",
        data: formdata,
        cache: false,
        success: function (response) {
            $(".show-master").html(response);
            $('.show-master-report').DataTable({
                destroy: true,
                "iDisplayLength": 100,
                "aoColumns": [
                    null, null, null, null,
                    {"bSortable": false}
                ],
                "order": [[0, "asc"]]
            });
//            window.location.href = response;
        }
    });
}
function show_report_listing() {
    var formdata = $("#report_listing").serialize();
    $.ajax({
        url: "functions.php",
        type: "POST",
        data: formdata,
        cache: false,
        success: function (response) {
            $(".show-report").html(response);
            $('.show-report-listing').DataTable({
                destroy: true,
                "iDisplayLength": 500,
                "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
                "aoColumns": [
                    null, null, null, null, null, null, null, null, null, null,
                    {"bSortable": false}
                ],
                "order": [[0, "asc"]]
            });
//            window.location.href = response;
        }
    });
}
function show_process_list() {
    var formdata = $("#process_list").serialize();
    $.ajax({
        url: "functions.php",
        type: "POST",
        data: formdata,
        cache: false,
        success: function (response) {
            $(".show-process-list").html(response);
            $('.show-report-listing').DataTable({
                destroy: true,
                "iDisplayLength": 100,
                "aoColumns": [
                    null, null, null, null, null, null, null, null,
                    {"bSortable": false}
                ],
                "order": [[0, "asc"]]
            });
//            window.location.href = response;
        }
    });
}
function export_summary_data_pdf() {
    var formdata = $("#summary_form").serialize();
    $.ajax({
        url: "functions.php",
        type: "POST",
        data: formdata,
        cache: false,
        success: function (response) {
            window.open(response, '_blank');
        }
    });
}
function export_detailed_data_pdf() {
    var formdata = $("#detailed_form").serialize();
    $.ajax({
        url: "functions.php",
        type: "POST",
        data: formdata,
        cache: false,
        success: function (response) {
            window.open(response, '_blank');
        }
    });
}
function deleteUser(user_id) {
    if (confirm("Are you sure want to remove this User?")) {
        $.ajax({
            url: "functions.php",
            type: "POST",
            data: {'action': 'delete_user', 'user_id': user_id},
            cache: false,
            success: function (response) {
                window.location.href = response;
            }
        });
    }
}
function deleteDivision(division_id) {
    if (confirm("Are you sure want to remove this Division? Have you Take backup all reports in excel?")) {
        $.ajax({
            url: "functions.php",
            type: "POST",
            data: {'action': 'delete_division', 'division_id': division_id},
            cache: false,
            success: function (response) {
                window.location.href = response;
            }
        });
    }
}
function deleteYear(year_id, division_id) {
    if (confirm("Are you sure want to remove this Period? Have you Take backup all reports in excel?")) {
        $.ajax({
            url: "functions.php",
            type: "POST",
            data: {'action': 'delete_year', 'year_id': year_id, 'division_id': division_id},
            cache: false,
            success: function (response) {
                window.location.href = response;
            }
        });
    }
}
function fill_year(division) {
    if (division != '') {
        $.ajax({
            url: "functions.php",
            type: "POST",
            data: {'action': 'fill_year', 'division': division},
            cache: false,
            success: function (response) {
                $("#year_dropdown").html(response);
            }
        });
    }
}