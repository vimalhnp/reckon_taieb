<?php
ob_start();

include_once './inner_header.php';
include_once './db_connection.php';

//if ($_SESSION['admin_user_type'] != 1) {
//    header("Location: admin_track_sheet_data_listing.php");
//}
$user_id = '-1';
//Check if Category is open in Edit mode or Insert mode
if (isset($_GET['user_id']) && $_GET['user_id'] != '') {
    // get data as its update form
    $user_id = $_GET['user_id'] = base64_decode($_GET['user_id']);
    $title = "Edit User";
    $action = "edit_user";
    $read_only = 'readonly';
    $basic_data = [];
    $res_data = mysqli_query($conn, "SELECT * FROM `user` WHERE user_id = '" . $user_id . "'");
    $res = mysqli_fetch_assoc($res_data);
} else {
    $title = "Add New User";
    $action = "add_user";
    $read_only = '';
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title"><?= $title ?></h4>
                    </div>
                    <div class="content">
                        <form class="form-horizontal" action="functions.php" method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="action" value="<?php echo $action; ?>">
                            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
                            <div class="row" style="margin: 0;">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Email ID/ Username</label>
                                        <input type="email" class="form-control" id="email_id" name="email_id" placeholder="Enter Email id" autocomplete="off" value="<?php echo (isset($res['email_id']) && $res['email_id'] != '') ? $res['email_id'] : ''; ?>" required="" autofocus="" <?php echo $read_only; ?> />
                                    </div>
                                </div>
                                <div class="col-md-5 col-md-offset-1">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" autocomplete="off" required=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <button type="submit" class="btn btn-info btn-fill ">Save</button>
                                <a href="user_manage.php" class="btn btn-default ">Cancel</a>
                            </div>

                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once './footer.php';
?>