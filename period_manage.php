<?php
ob_start();

include_once './inner_header.php';
include_once './db_connection.php';

global $conn;
if ($_SESSION['is_admin'] != 1) {
    header("Location:master_reports_listing.php");
}
if (!isset($_GET['did']) || $_GET['did'] == '') {
    header("Location:division_manage.php");
} else {
    $division_id = base64_decode($_GET['did']);
}
is_user_active();
set_user_active_time();

$res_data = mysqli_query($conn, "SELECT `year`.*,d.division FROM `year` 
                                JOIN division as d ON (d.division_id = `year`.division_id)
                                WHERE `year`.`status`=1 AND `year`.`division_id`='" . $division_id . "' ORDER BY `year`.year_id DESC");
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add New Period</h4>
                    </div>
                    <div class="content">
                        <form class="form-horizontal" action="functions.php" method="post">
                            <input type="hidden" name="action" value="add_year">
                            <input type="hidden" name="division_id" value="<?php echo $division_id; ?>">
                            <div class="row" style="margin: 0;">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Year</label>
                                        <input type="text" class="form-control" id="year" name="year" placeholder="Enter year (E.G. : 2017, 1718 etc)" required="" autofocus="" />
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <button type="submit" class="btn btn-info btn-fill ">Add Period</button>
                                <a href="division_manage.php" class="btn btn-default ">Back To Division</a>
                            </div>

                            <div class="clearfix"></div>
                        </form>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover ">
                            <thead>
                            <th>ID</th>
                            <th>Division</th>
                            <th>Year</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                if (mysqli_num_rows($res_data) > 0) {
                                    while ($data_row = mysqli_fetch_assoc($res_data)) {
                                        ?>
                                        <tr id="record_<?php echo $data_row['year_id']; ?>">
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo $data_row['division']; ?></td>
                                            <td><?php echo $data_row['year']; ?></td>
                                            <td>
                                                <a href="javascript:void(0);" onclick="deleteYear(<?php echo ($data_row['year_id']); ?>,<?php echo ($data_row['division_id']); ?>)" title="Delete Period" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once './footer.php';
?>