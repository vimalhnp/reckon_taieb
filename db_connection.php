<?php

ob_start();

header("Cache-control: private, no-cache");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Pragma: no-cache");
header("Cache: no-cahce");
ini_set('max_execution_time', 90000);
ini_set("memory_limit", -1);
date_default_timezone_set("Asia/Kolkata");

if ($_SERVER['HTTP_HOST'] == "localhost") {
    $isLocal = true;
} else {
    $isLocal = false;
}

if ($isLocal == true) {
    DEFINE('DB_SERVER', 'localhost'); // eg, localhost - should not be empty for productive servers
    DEFINE('DB_SERVER_USERNAME', 'root');
    DEFINE('DB_SERVER_PASSWORD', '');
    DEFINE('DB_DATABASE', 'taieb');
    DEFINE('SITE_FOLDER', 'taieb/');

    DEFINE("SITE_URL_REMOTE", "http://" . $_SERVER['HTTP_HOST'] . '/' . SITE_FOLDER);
} else {
    DEFINE('DB_SERVER', 'localhost');
    DEFINE('DB_SERVER_USERNAME', 'xxxxxxxxxxxxxxxxxxxxxx');
    DEFINE('DB_SERVER_PASSWORD', 'xxxxxxxxxxxxxxxxxxxxxx');
    DEFINE('DB_DATABASE', 'taieb');
    DEFINE('SITE_FOLDER', 'taieb/');
    DEFINE('SITE_URL_REMOTE', 'http://' . $_SERVER['HTTP_HOST'] . '/' . SITE_FOLDER);
}
DEFINE('ENCRYPT_KEY', 'vd@_applications_@');
DEFINE('CDN_MJ_IMAGEURL', 'https://' . $_SERVER['HTTP_HOST'] . '/' . SITE_FOLDER . 'images');

DEFINE('PAGINATION_LIMIT', 50);
DEFINE('ORDER_PAGINATION_LIMIT', 100);
DEFINE('PDF_PATH', 'uploads/');

DEFINE("SITE_ROOT", $_SERVER['DOCUMENT_ROOT'] . '/' . SITE_FOLDER);

$conn = mysqli_connect(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD, DB_DATABASE) or die('Error connecting to database.');
$conn->set_charset("utf8");

function send_mail_with_headers($subject, $from, $recipient, $mess, $isHTML = FALSE, $filearr = Array(), $path = "", $replyto = '') {
    //set the message content type
    $content_type = "text/plain";
    if ($isHTML == TRUE) {
        $content_type = "text/html";
    }
    if (is_array($recipient)) {
        $to = $recipient[0];
        $cc = $recipient[1];
        $bcc = $recipient[2];
    } else {
        $to = $recipient;
        $cc = "";
        $bcc = "";
    }
    //set the header
    $headers = "";
    if (count($filearr) > 0) {// USE multipart mime message to send mail with attachment
        //unique mime boundry seperater
        $mime_boundary_value = md5(uniqid(time()));
        //set the headers
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: multipart/mixed; boundary=\"$mime_boundary_value\";\r\n";
        $headers .= "If you are reading this, then upgrade your e-mail client to support MIME.\r\n";
        //set the message
        if ($mess <> "") {
            $mess = "--$mime_boundary_value\n" .
                    "Content-Type: $content_type; charset=\"iso-8859-1\"\n" .
                    "Content-Transfer-Encoding: 7bit\n\n" .
                    $mess . "\n\n";
        }
        for ($i = 0; $i < count($filearr); $i++) {
            // if the upload succeded, the file will exist
            if (file_exists($filearr[$i])) {
                $mess .= "--$mime_boundary_value\n";
                $mess .= "Content-Type: application/pdf; name=\"{$filearr[$i]}\"\n";
                $mess .= "Content-Disposition: attachment; filename=\"{$filearr[$i]}\"\n";
                $mess .= "Content-Transfer-Encoding: base64\n\n";
                //read file data
                $file = fopen($filearr[$i], 'rb');
                $data = fread($file, filesize($filearr[$i]));
                fclose($file);
                //encode file data
                $data = chunk_split(base64_encode($data));
                $mess .= $data . "\n\n";
            }
        }
        $mess .= "--$mime_boundary_value--\n"; //end message
    } else {
        //$headers.= 'MIME-Version: 1.0' . "\r\n";
        $headers.='Content-Type: ' . $content_type . '; charset=iso-8859-1' . "\r\n";
    }
    //    $headers.='To: ' . $to . "\r\n";
    $headers.='From: ' . $from . "\r\n";
    $bcc != '' ? $headers.="Bcc: $bcc \r\n" : '';
    $cc != '' ? $headers.="cc: $cc \r\n" : '';
    $headers.='Bounce-to: ' . $recipient[2] . "\r\n";
    if (trim($replyto) != "") {
        $headers.='Reply-To: ' . $replyto . "\r\n";
        $headers.='Return-Path: ' . $replyto . "\r\n";
    }
    //    $fp = fopen("maillog.txt", "a");
    //    fwrite($fp, "Header start here: \n" . $headers . "\n Message start here: \n" . $mess);
    //    fclose($fp);
    $response = mail($to, $subject, $mess, $headers, '-f' . $from); //this function will send mail
    return $response;
    // return "Recipient : ".$recipient."<hr>Subject: ".$subject."<hr>Message: ".$mess."<hr>Header: ".$headers;
}

function get_permissions($user_id = 0, $permission_name) {
    global $conn;
    
    if ($user_id == 0) {
        return array();
    }
    
    $sel_email = mysqli_query($conn, "SELECT `$permission_name` FROM user WHERE user_id = '" . mysqli_real_escape_string($conn, $user_id) . "'");
    if (mysqli_num_rows($sel_email) > 0) {
        while ($masterdata = mysqli_fetch_assoc($sel_email)) {
            if (isset($masterdata[$permission_name]) && $masterdata[$permission_name] != '') {
                return json_decode($masterdata[$permission_name], TRUE);
            }
        }
    } else {
        return array();
    }
}

function user_log($user_id, $activity) {
    global $conn;
    
    $current_date = date('Y-m-d H:i:s');
    $insert = "INSERT INTO user_log  (`user_id`,`activity`,`status`,`added_on`,`modified_on`) VALUES ('" . $user_id . "','" . $activity . "','1','" . $current_date . "','" . $current_date . "')";
    $result = mysqli_query($conn, $insert);
}

function is_user_active() {
    if($_SESSION['is_inactivity'] < time()) {
        user_log($_SESSION['user_id'], 'time-out due inactivity');
        unset($_SESSION['user_id']);
        unset($_SESSION['is_inactivity']);
        unset($_SESSION['username']);
        unset($_SESSION['email_id']);
        unset($_SESSION['is_admin']);
        header("Location:index.php");
    }
}

function set_user_active_time() {
    $_SESSION['is_inactivity'] = time() + 6000;
}

function get_divisions() {
    global $conn;

    $sel_email = mysqli_query($conn, "SELECT * FROM division WHERE `status` = 1");
    if (mysqli_num_rows($sel_email) > 0) {
        $returnArr = [];
        while ($masterdata = mysqli_fetch_assoc($sel_email)) {
            $returnArr[$masterdata['division_id']] = $masterdata['division'];
        }
        return $returnArr;
    } else {
        return array();
    }
}

//ob_end_flush();
?>