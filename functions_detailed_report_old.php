<?php

session_start();

header("Cache-control: private, no-cache");
header("Pragma: no-cache");
header("Cache: no-cahce");
ini_set('max_execution_time', 90000);
ini_set("memory_limit", -1);
//header("Content-length: 500M");
ini_set('auto_detect_line_endings', TRUE);

require_once './db_connection.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

//use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

extract($_REQUEST);
global $conn;

if (isset($action) && $action == "login_user") {
    $sel_email = mysqli_query($conn, "SELECT * FROM user WHERE username = '" . mysqli_real_escape_string($conn, $username) . "' AND `password`='" . mysqli_real_escape_string($conn, md5($password)) . "' AND `status`=1");
    if (mysqli_num_rows($sel_email) > 0) {
        $data = mysqli_fetch_assoc($sel_email);
        $_SESSION['user_id'] = $data['user_id'];
        $_SESSION['username'] = $data['username'];
        $_SESSION['email_id'] = $data['email_id'];
        echo 'success';
    } else {
        echo 'fail';
    }
} else if (isset($action) && $action == "logout_user") {
    unset($_SESSION['user_id']);
    unset($_SESSION['username']);
    unset($_SESSION['email_id']);
    echo 'success';
} elseif (isset($action) && $action == "register_user") {
    $sel_email = mysqli_query($conn, "SELECT * FROM user WHERE username = '" . mysqli_real_escape_string($conn, $username) . "'");
    if (mysqli_num_rows($sel_email) > 0) {
        echo 'Already Username Exist';
    } else {
        $current_date = date('Y-m-d H:i:s');
        $insert = "INSERT INTO user (`username`,`password`,`email_id`,`added_on`,`modified_on`) VALUES ('" . $username . "','" . md5($password) . "','" . $email_id . "','" . $current_date . "','" . $current_date . "')";
        $result = mysqli_query($conn, $insert);
        if ($result) {
            echo 'Successful Registration';
        }
    }
} elseif (isset($action) && $action == "add_report") {
    $current_date = date('Y-m-d H:i:s');
    $insert = "INSERT INTO reports (`date`,`ref`,`account`,`payee`,`memo`,`amount`,`exp_head`,`added_on`,`modified_on`,`division`,`year`) VALUES ('" . mysqli_real_escape_string($conn, $date) . "','" . mysqli_real_escape_string($conn, $ref) . "','" . mysqli_real_escape_string($conn, $account) . "','" . mysqli_real_escape_string($conn, strtoupper($payee)) . "','" . mysqli_real_escape_string($conn, $memo) . "','" . mysqli_real_escape_string($conn, $amount) . "','" . mysqli_real_escape_string($conn, $exp_head) . "','" . $current_date . "','" . $current_date . "','" . mysqli_real_escape_string($conn, $division) . "','" . mysqli_real_escape_string($conn, $year) . "')";
    $result = mysqli_query($conn, $insert);
    if ($result) {
        header("Location:reports_listing.php");
    }
} elseif (isset($action) && $action == "edit_report") {
    $current_date = date('Y-m-d H:i:s');
    $reports_id = $_POST['reports_id'];
    unset($_POST['reports_id']);
    unset($_POST['action']);
    $updateQ = '';
    if (count($_POST) > 0) {
        foreach ($_POST as $field_name => $field_value) {
            if ($field_name == 'payee') {
                $field_value = strtoupper($field_value);
            }
            $updateQ.='`' . $field_name . '` ="' . mysqli_real_escape_string($conn, $field_value) . '", ';
        }
    }
    $updateQ = rtrim($updateQ, ", ");

    $update = "UPDATE `reports` SET $updateQ WHERE reports_id = '" . mysqli_real_escape_string($conn, $reports_id) . "'";
    $result = mysqli_query($conn, $update);
    if ($result) {
        header("Location:reports_listing.php?success");
    } else {
        header("Location:reports_listing.php?fail");
    }
} elseif (isset($action) && $action == "upload_file") {
    $target_dir = "uploads/";
    $target_file = $target_dir . time() . basename($_FILES["report_file"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


// Check file size
    if ($_FILES["report_file"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
// Allow certain file formats
    if ($imageFileType != "xlsx") {
        echo "Sorry, only Excel (.xlsx) file allowed.";
        $uploadOk = 0;
    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["report_file"]["tmp_name"], $target_file)) {
            basename($_FILES["report_file"]["name"]);
            require 'vendor/autoload.php';


            $reader = new Xlsx();
            $spreadsheet = $reader->load($target_file);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();


            $headings = array_shift($sheetData);
            //convert headings to small case and remove space
            array_walk($headings, function(&$val) {
                $val = str_replace(' ', '_', strtolower($val));
            });

            // combine all data so key as value setup
            array_walk($sheetData, function (&$row) use ($headings) {
                $row = array_combine($headings, $row);
            }
            );
            $current_date = date('Y-m-d H:i:s');
            foreach ($sheetData as $key => $value) {
                $value1 = array_values($value);
                $value1 = array_filter($value1);

                if (count($value1) > 0) {
                    if ($import_action == 'append') {

                        $q = "INSERT INTO reports SET ";

                        foreach ($value as $fields => $data) {
                            if ($fields == 'payee') {
                                $data = strtoupper($data);
                            }
                            if ($fields != '') {
                                if ($fields != 'payee') {
                                    $data = trim(htmlspecialchars($data, ENT_IGNORE));
                                }
                                $q.="`$fields` = '" . mysqli_real_escape_string($conn, $data) . "',";
                            }
                        }
                        $q.="`division`='" . mysqli_real_escape_string($conn, trim($division)) . "',";
                        $q.="`year`='" . mysqli_real_escape_string($conn, trim($year)) . "',";
                        $q.="`added_on` = '" . $current_date . "',";
                        $q.="`modified_on` = '" . $current_date . "'";
                        $q = rtrim($q, ",") . ";";
                        $result = mysqli_query($conn, $q);
                    } elseif ($import_action == 'replace') {
                        $q = "UPDATE reports SET ";
                        $where = ' WHERE ';
                        $whereArr = [];
                        foreach ($value as $fields => $data) {
                            if ($fields == 'payee') {
                                $data = strtoupper($data);
                                $whereArr[] = " `payee`='" . mysqli_real_escape_string($conn, $data) . "'";
                            }
                            if ($fields == 'date') {
                                $whereArr[] = " `date`='" . mysqli_real_escape_string($conn, $data) . "'";
                            }
                            if ($fields != '' && $fields != 'payee' && $fields != 'date') {
                                $q.="`$fields` = '" . mysqli_real_escape_string($conn, trim(htmlspecialchars($data, ENT_IGNORE))) . "',";
                            }
                        }
                        $where.= implode(" AND ", $whereArr);
                        $where .=" AND `division`='" . mysqli_real_escape_string($conn, trim($division)) . "' ";
                        $where .=" AND `year`='" . mysqli_real_escape_string($conn, trim($year)) . "' ";

                        $q.="`modified_on` = '" . $current_date . "'";
                        $q = rtrim($q, ",");
                        $q = $q . $where;

                        $result = mysqli_query($conn, $q);
                    }
                }
            }
            header("Location:reports_listing.php");
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
} elseif (isset($action) && $action == "master_upload_file") {
    $target_dir = "uploads/";
    $target_file = $target_dir . time() . basename($_FILES["report_file"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


// Check file size
    if ($_FILES["report_file"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
// Allow certain file formats
    if ($imageFileType != "xlsx") {
        echo "Sorry, only Excel (.xlsx) file allowed.";
        $uploadOk = 0;
    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["report_file"]["tmp_name"], $target_file)) {
            basename($_FILES["report_file"]["name"]);
            require 'vendor/autoload.php';


            $reader = new Xlsx();
            $spreadsheet = $reader->load($target_file);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();


            $headings = array_shift($sheetData);
            //convert headings to small case and remove space
            array_walk($headings, function(&$val) {
                $val = str_replace(' ', '_', strtolower($val));
            });

            // combine all data so key as value setup
            array_walk($sheetData, function (&$row) use ($headings) {
                $row = array_combine($headings, $row);
            }
            );
            $current_date = date('Y-m-d H:i:s');
            foreach ($sheetData as $key => $value) {
                $value1 = array_values($value);
                $value1 = array_filter($value1);

                if (count($value1) > 0) {
                    if ($import_action == 'append') {

                        $q = "INSERT INTO master_reports SET ";

                        foreach ($value as $fields => $data) {
                            if ($fields == 'payee') {
                                $data = strtoupper($data);
                            }
                            if ($fields != '') {
                                if ($fields != 'payee') {
                                    $data = trim(htmlspecialchars($data, ENT_IGNORE));
                                }
                                $q.="`$fields` = '" . mysqli_real_escape_string($conn, $data) . "',";
                            }
                        }
                        $q.="`division`='" . mysqli_real_escape_string($conn, trim($division)) . "',";
//                        $q.="`year`='" . mysqli_real_escape_string($conn, trim($year)) . "',";
                        $q.="`added_on` = '" . $current_date . "',";
                        $q.="`modified_on` = '" . $current_date . "'";
                        $q = rtrim($q, ",") . ";";
                        $result = mysqli_query($conn, $q);
                    } elseif ($import_action == 'replace') {
                        $q = "UPDATE master_reports SET ";
                        $where = ' WHERE ';
                        foreach ($value as $fields => $data) {
                            if ($fields == 'payee') {
                                $data = strtoupper($data);
                                $where .=" `payee`='" . mysqli_real_escape_string($conn, $data) . "'";
                            }
                            if ($fields != '' && $fields != 'payee') {
                                $q.="`$fields` = '" . mysqli_real_escape_string($conn, trim(htmlspecialchars($data, ENT_IGNORE))) . "',";
                            }
                        }
                        $where .=" AND `division`='" . mysqli_real_escape_string($conn, trim($division)) . "' ";
//                        $where .=" AND `year`='" . mysqli_real_escape_string($conn, trim($year)) . "' ";
                        $q.="`modified_on` = '" . $current_date . "'";
                        $q = rtrim($q, ",");
                        $q = $q . $where;

                        $result = mysqli_query($conn, $q);
                    }
                }
            }
            header("Location:master_reports_listing.php");
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
} elseif (isset($action) && $action == "change_permission") {
    $current_date = date('Y-m-d H:i:s');
    $state1 = '0';
    if ($state == 'true') {
        $state1 = '1';
    }
    $update = "UPDATE `user` SET `$field` = '" . mysqli_real_escape_string($conn, $state1) . "' WHERE user_id='" . mysqli_real_escape_string($conn, $user_id) . "'";
    $result = mysqli_query($conn, $update);
    echo 'success';
} elseif (isset($action) && $action == "change_permission_new") {
    $current_date = date('Y-m-d H:i:s');
    $upload_permission = json_encode($_POST['upload_permission'], TRUE);
    $process_permission = json_encode($_POST['process_permission'], TRUE);
    $report_permission = json_encode($_POST['report_permission'], TRUE);
    $update = "UPDATE `user` SET `upload_permission` = '" . mysqli_real_escape_string($conn, $upload_permission) . "',`process_permission` = '" . mysqli_real_escape_string($conn, $process_permission) . "',`report_permission` = '" . mysqli_real_escape_string($conn, $report_permission) . "' WHERE user_id='" . mysqli_real_escape_string($conn, $user_id) . "'";

    $result = mysqli_query($conn, $update);
    echo 'success';
} elseif (isset($action) && $action == "add_user") {
    $updateQ = '';
    if (isset($password) && $password != '') {
        $updateQ.=" , `password`='" . mysqli_real_escape_string($conn, md5($password)) . "' ";
    }
    if (!isset($write_permission)) {
        $write_permission = 0;
    }
    if (!isset($read_permission)) {
        $read_permission = 0;
    }
    $current_date = date('Y-m-d H:i:s');
    $insert = "INSERT INTO user SET `username`='" . mysqli_real_escape_string($conn, $email_id) . "',`email_id`='" . mysqli_real_escape_string($conn, $email_id) . "',`is_admin`='0',`login_first_time`='1',`read_permission`='" . mysqli_real_escape_string($conn, $read_permission) . "',`write_permission`='" . mysqli_real_escape_string($conn, $write_permission) . "',`division`='" . mysqli_real_escape_string($conn, $division) . "',`added_on`='" . mysqli_real_escape_string($conn, $current_date) . "',`modified_on`='" . mysqli_real_escape_string($conn, $current_date) . "' " . $updateQ;
    $result = mysqli_query($conn, $insert);
    if ($result) {
        header("Location:user_manage.php");
    }
} elseif (isset($action) && $action == "edit_user") {
    $current_date = date('Y-m-d H:i:s');
    $user_id = $_POST['user_id'];
    unset($_POST['user_id']);
    unset($_POST['action']);
    $updateQ = '';
    if (isset($password) && $password != '') {
        $updateQ.=" `password`='" . mysqli_real_escape_string($conn, md5($password)) . "', ";
    }
    if (!isset($_POST['write_permission'])) {
        $_POST['write_permission'] = 0;
    }
    if (!isset($_POST['read_permission'])) {
        $_POST['read_permission'] = 0;
    }
    unset($_POST['password']);
    if (count($_POST) > 0) {
        foreach ($_POST as $field_name => $field_value) {
            $updateQ.='`' . $field_name . '` ="' . mysqli_real_escape_string($conn, $field_value) . '", ';
        }
    }

    $updateQ = rtrim($updateQ, ", ");

    $update = "UPDATE `user` SET $updateQ WHERE user_id = '" . mysqli_real_escape_string($conn, $user_id) . "'";
    $result = mysqli_query($conn, $update);
    if ($result) {
        header("Location:user_manage.php?success");
    } else {
        header("Location:user_manage.php?fail");
    }
} elseif (isset($action) && $action == "add_master_report") {
    $current_date = date('Y-m-d H:i:s');
    $insert = "INSERT INTO master_reports (`payee`,`exp_head`,`added_on`,`modified_on`,`division`) VALUES ('" . mysqli_real_escape_string($conn, strtoupper($payee)) . "','" . mysqli_real_escape_string($conn, $exp_head) . "','" . $current_date . "','" . $current_date . "','" . mysqli_real_escape_string($conn, $division) . "')";
    $result = mysqli_query($conn, $insert);
    if ($result) {
        header("Location:master_reports_listing.php");
    }
} elseif (isset($action) && $action == "edit_master_report") {
    $current_date = date('Y-m-d H:i:s');
    $reports_id = $_POST['master_reports_id'];
    unset($_POST['master_reports_id']);
    unset($_POST['action']);
    $updateQ = '';
    if (count($_POST) > 0) {
        foreach ($_POST as $field_name => $field_value) {
            if ($field_name == 'payee') {
                $field_value = strtoupper($field_value);
            }
            $updateQ.='`' . $field_name . '` ="' . mysqli_real_escape_string($conn, $field_value) . '", ';
        }
    }
    $updateQ = rtrim($updateQ, ", ");

    $update = "UPDATE `master_reports` SET $updateQ WHERE master_reports_id = '" . mysqli_real_escape_string($conn, $reports_id) . "'";
    $result = mysqli_query($conn, $update);
    if ($result) {
        header("Location:master_reports_listing.php?success");
    } else {
        header("Location:master_reports_listing.php?fail");
    }
} elseif (isset($action) && $action == "process_expence_data") {
    $current_date = date('Y-m-d H:i:s');



    $getreports = mysqli_query($conn, "SELECT * FROM `reports` WHERE `exp_head` ='' AND  `status`=1");
    if (mysqli_num_rows($getreports) > 0) {

        //get master data into array 
        $masterdataArr = [];
        $getmasterreports = mysqli_query($conn, "SELECT * FROM `master_reports` WHERE `status` = 1");
        if (mysqli_num_rows($getmasterreports) > 0) {
            while ($masterdata = mysqli_fetch_assoc($getmasterreports)) {
                $masterdataArr[$masterdata['payee']] = $masterdata['exp_head'];
            }
        }
        $entered_masterdata = [];
        while ($data = mysqli_fetch_assoc($getreports)) {
            if (key_exists($data['payee'], $masterdataArr)) {
                // update expense table from master table
                $update = "UPDATE `reports` SET `exp_head` ='" . $masterdataArr[$data['payee']] . "' WHERE reports_id = '" . mysqli_real_escape_string($conn, $data['reports_id']) . "'";
                $result = mysqli_query($conn, $update);
            } else {
                if (!in_array($data['payee'], $entered_masterdata)) {
                    //enter data into master table
                    $insert = "INSERT INTO master_reports (`payee`,`added_on`,`modified_on`,`division`) VALUES ('" . mysqli_real_escape_string($conn, strtoupper($data['payee'])) . "','" . $current_date . "','" . $current_date . "','" . mysqli_real_escape_string($conn, strtoupper($data['division'])) . "')";
                    $result = mysqli_query($conn, $insert);
                    $entered_masterdata[] = $data['payee'];
                }
            }
        }
    }


    if ($result) {
        header("Location:process_list.php?success");
    } else {
        header("Location:process_list.php?fail");
    }
} elseif (isset($action) && $action == "export_summary_data") {

    $getmasterreports = mysqli_query($conn, "SELECT exp_head,amount,account FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "'");
    $creditArr = $creditArr2 = [];  //  If it is negative, it is identified as the credit balance
    $debitArr = $debitArr2 = [];   //  If it is positive, it is identified as the debit balance
    $debitTotal = 0;
    $creditTotal = 0;
    if (mysqli_num_rows($getmasterreports) > 0) {
        $file_name = $division . $year;
        while ($masterdata = mysqli_fetch_assoc($getmasterreports)) {
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($amt < 0) {
                $creditArr[$masterdata['exp_head']][] = abs($amt);
                $creditArr2[$masterdata['account']][] = abs($amt);
//                $creditTotal+=abs($amt);
            } else {
                $debitArr[$masterdata['exp_head']][] = abs($amt);
                $debitArr2[$masterdata['account']][] = abs($amt);
//                $debitTotal+=abs($amt);
            }
        }
    } else {
        header("Location:summary_table.php?fail");
    }
    $getexpheads = mysqli_query($conn, "SELECT DISTINCT exp_head FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `account`!=''  AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY exp_head, date, amount");
    $table_exp = '<table cellpadding="5" cellmargin="5" style="width:100%;border:1px solid black;">';
    $table_exp.='<tr class="border_bottom"><td colspan="4">Summary table for ' . $file_name . '</td></tr>';
    $table_exp.='<tr class="border_bottom"><td>S No</td><td>Exp Head</td><td>Debit</td><td>Credit</td></tr>';
    $i = 1;
    while ($exp_heads = mysqli_fetch_assoc($getexpheads)) {
        $debitAmt = (isset($debitArr[$exp_heads['exp_head']]) && count($debitArr[$exp_heads['exp_head']]) > 0) ? array_sum($debitArr[$exp_heads['exp_head']]) : "-";
        $creditAmt = (isset($creditArr[$exp_heads['exp_head']]) && count($creditArr[$exp_heads['exp_head']]) > 0) ? array_sum($creditArr[$exp_heads['exp_head']]) : "-";
        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($debitAmt - $creditAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $debitAmt = '-';
                $creditAmt = abs($diff);
            } else {
                $creditAmt = '-';
                $debitAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);

        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $table_exp.='<tr>';
        $table_exp.='<td>' . $i . '</td>';
        $table_exp.='<td>' . $exp_heads['exp_head'] . '</td>';
        $table_exp.='<td>' . $debitAmt . '</td>';
        $table_exp.='<td>' . $creditAmt . '</td>';
        $table_exp.='</tr>';
        $i++;
    }

    $getaccount = mysqli_query($conn, "SELECT DISTINCT account FROM `reports` WHERE `status` = 1 AND `account`!='' AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY account");
    while ($accounts = mysqli_fetch_assoc($getaccount)) {
        $creditAmt = (isset($debitArr2[$accounts['account']]) && count($debitArr2[$accounts['account']]) > 0) ? array_sum($debitArr2[$accounts['account']]) : "-";
        $debitAmt = (isset($creditArr2[$accounts['account']]) && count($creditArr2[$accounts['account']]) > 0) ? array_sum($creditArr2[$accounts['account']]) : "-";

        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($creditAmt - $debitAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $creditAmt = '-';
                $debitAmt = abs($diff);
            } else {
                $debitAmt = '-';
                $creditAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);
        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $table_exp.='<tr>';
        $table_exp.='<td>' . $i . '</td>';
        $table_exp.='<td>' . $accounts['account'] . '</td>';
        $table_exp.='<td>' . ($debitAmt) . '</td>';
        $table_exp.='<td>' . ($creditAmt) . '</td>';
        $table_exp.='</tr>';
        $i++;
    }
    if (abs($debitTotal) > 0) {
        $debitTotal = "$ " . number_format($debitTotal, 2);
    }
    if (abs($creditTotal) > 0) {
        $creditTotal = "$ " . number_format($creditTotal, 2);
    }
    $table_exp.='<tr><td></td><td></td><td><hr/></td><td><hr/></td></tr>';
    $table_exp.='<tr>';
    $table_exp.='<td> </td>';
    $table_exp.='<td>Total</td>';
    $table_exp.='<td>' . $debitTotal . '</td>';
    $table_exp.='<td>' . $creditTotal . '</td>';
    $table_exp.='</tr>';
    $table_exp.='<tr><td></td><td></td><td><hr/></td><td><hr/></td></tr>';
    $table_exp.='</table>';

    $html = '<html>
                <head>
                    <style>
                        @page {
                            margin-header: 10mm;
                            margin-footer: 10mm;
                            margin:15px;
                        }
                        body {font-family: sans-serif;
                              font-size: 10pt;
                        }
                        p {	margin: 0pt; }
                        .border_bottom td{ border-bottom:1px solid black; }
                    </style>
                </head>
                <body style="background-color: #eaeaea;">
                    <!--mpdf
                    <htmlpagefooter name="myfooter">
                    <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
                    Page {PAGENO} of {nb}
                    </div>
                    </htmlpagefooter>
                    <sethtmlpagefooter name="myfooter" value="off" />
                    mpdf-->
                    <div style="width: 100%; margin: 0; padding: 0;">
                        <div style="background-color:#fff;width:100%;">
                            ' . $table_exp . '
                        </div>
                    </div>
                </body>
            </html>';


    define('_MPDF_PATH', './mpdf60/');
    include("./mpdf60/mpdf.php");

    $mpdf = new mPDF('d', 'A4', '', '', 0, 0, 0, 0, 5, 5);
    $mpdf->SetProtection(array('print'));
    $mpdf->SetTitle($file_name . " " . date('Y-m-d'));
    $mpdf->SetAuthor("Vimal Patel");
    $mpdf->WriteHTML($html);
    $pdffile = $file_name . ".pdf";
//    $mpdf->Output();

    $mpdf->Output(PDF_PATH . $pdffile);
    header("Location:" . PDF_PATH . $pdffile);
    exit();
} elseif (isset($action) && $action == "export_summary_data_excel") {

    require 'vendor/autoload.php';

    $spreadsheet = new Spreadsheet();  /* ----Spreadsheet object----- */
    $Excel_writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);  /* ----- Excel (Xls) Object */
    $spreadsheet->setActiveSheetIndex(0);
    $activeSheet = $spreadsheet->getActiveSheet();


    $getmasterreports = mysqli_query($conn, "SELECT exp_head,amount,account FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "'");
    $creditArr = $creditArr2 = [];  //  If it is negative, it is identified as the credit balance
    $debitArr = $debitArr2 = [];   //  If it is positive, it is identified as the debit balance
    $debitTotal = 0;
    $creditTotal = 0;
    
    if (mysqli_num_rows($getmasterreports) > 0) {
        $file_name = $division . $year;
        while ($masterdata = mysqli_fetch_assoc($getmasterreports)) {
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($amt < 0) {
                $creditArr[$masterdata['exp_head']][] = abs($amt);
                $creditArr2[$masterdata['account']][] = abs($amt);
//                $creditTotal+=abs($amt);
            } else {
                $debitArr[$masterdata['exp_head']][] = abs($amt);
                $debitArr2[$masterdata['account']][] = abs($amt);
//                $debitTotal+=abs($amt);
            }
        }
    } else {
        header("Location:summary_table.php?fail");
    }
    $getexpheads = mysqli_query($conn, "SELECT DISTINCT exp_head FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `account`!=''  AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY exp_head, date, amount");
    $activeSheet->setCellValue('A1', 'S No')->getStyle('A1')->getFont()->setBold(true);
    $activeSheet->setCellValue('B1', 'Exp Head')->getStyle('B1')->getFont()->setBold(true);
    $activeSheet->setCellValue('C1', 'Debit')->getStyle('C1')->getFont()->setBold(true);
    $activeSheet->setCellValue('D1', 'Credit')->getStyle('D1')->getFont()->setBold(true);
    $i = 1;
    $row = 2;
    while ($exp_heads = mysqli_fetch_assoc($getexpheads)) {
        $debitAmt = (isset($debitArr[$exp_heads['exp_head']]) && count($debitArr[$exp_heads['exp_head']]) > 0) ? array_sum($debitArr[$exp_heads['exp_head']]) : "-";
        $creditAmt = (isset($creditArr[$exp_heads['exp_head']]) && count($creditArr[$exp_heads['exp_head']]) > 0) ? array_sum($creditArr[$exp_heads['exp_head']]) : "-";
        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($debitAmt - $creditAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $debitAmt = '-';
                $creditAmt = abs($diff);
            } else {
                $creditAmt = '-';
                $debitAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);

        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $activeSheet->setCellValue('A' . $row, $i);
        $activeSheet->setCellValue('B' . $row, $exp_heads['exp_head']);
        $activeSheet->setCellValue('C' . $row, $debitAmt);
        $activeSheet->setCellValue('D' . $row, $creditAmt);
        $row++;
        $i++;
    }

    $getaccount = mysqli_query($conn, "SELECT DISTINCT account FROM `reports` WHERE `status` = 1 AND `account`!='' AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY account");
    while ($accounts = mysqli_fetch_assoc($getaccount)) {
        $creditAmt = (isset($debitArr2[$accounts['account']]) && count($debitArr2[$accounts['account']]) > 0) ? array_sum($debitArr2[$accounts['account']]) : "-";
        $debitAmt = (isset($creditArr2[$accounts['account']]) && count($creditArr2[$accounts['account']]) > 0) ? array_sum($creditArr2[$accounts['account']]) : "-";

        $diff = 0;
        if (abs($debitAmt) > 0 && abs($creditAmt) > 0) {
            $diff = (number_format($creditAmt - $debitAmt, 2));
            $diff = str_replace(",", "", $diff);
            if ($diff < 0) {
                $creditAmt = '-';
                $debitAmt = abs($diff);
            } else {
                $debitAmt = '-';
                $creditAmt = $diff;
            }
        }

        $creditTotal+=abs($creditAmt);
        $debitTotal+=abs($debitAmt);
        if (abs($debitAmt) > 0) {
            $debitAmt = "$ " . number_format($debitAmt, 2);
        }
        if (abs($creditAmt) > 0) {
            $creditAmt = "$ " . number_format($creditAmt, 2);
        }
        $activeSheet->setCellValue('A' . $row, $i);
        $activeSheet->setCellValue('B' . $row, $accounts['account']);
        $activeSheet->setCellValue('C' . $row, $debitAmt);
        $activeSheet->setCellValue('D' . $row, $creditAmt);
        $row++;
        $i++;
    }
    if (abs($debitTotal) > 0) {
        $debitTotal = "$ " . number_format($debitTotal, 2);
    }
    if (abs($creditTotal) > 0) {
        $creditTotal = "$ " . number_format($creditTotal, 2);
    }
    
    $activeSheet->setCellValue('A' . $row, '');
    $activeSheet->setCellValue('B' . $row, 'Total');
    $activeSheet->setCellValue('C' . $row, $debitTotal)->getStyle('C' . $row)->getFont()->setBold(true);
    $activeSheet->setCellValue('D' . $row, $creditTotal)->getStyle('D' . $row)->getFont()->setBold(true);


    
    $filename = 'Summary table for ' . $file_name  ;
//    header('Content-Type: application/vnd.ms-excel');
//    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"'); /* -- $filename is  xsl filename --- */
//    header('Cache-Control: max-age=0');
    $Excel_writer->save('uploads/' . $filename . '.xlsx');
    echo 'uploads/' . $filename . '.xlsx';
} elseif (isset($action) && $action == "export_detailed_data") {
    $getmasterreports = mysqli_query($conn, "SELECT * FROM `reports` WHERE `status` = 1 AND `exp_head`!='' AND `account`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY exp_head, date, amount");
    $creditArr = $creditArr2 = [];  //  If it is negative, it is identified as the credit balance
    $debitArr = $debitArr2 = [];   //  If it is positive, it is identified as the debit balance
    $debitTotal = 0;
    $creditTotal = 0;
    $exp_creditTotal = 0;
    $exp_debitTotal = 0;
//    $reportsArr = [];

    if (mysqli_num_rows($getmasterreports) > 0) {
        $file_name = $division . $year;
        $table_exp = '<table cellpadding="5" cellmargin="5" style="width:100%;border:1px solid black;">';
        $table_exp.='<tr class="border_bottom"><td colspan="7" style="background-color:#c5e1b1;">Detailed Report for ' . $division . ' for ' . $year . '</td></tr>';
        $table_exp.='<tr class="border_bottom"><td>Exp Head</td><td>Date</td><td>Ref</td><td>Payee</td><td>Debit</td><td>Credit</td><td>Balance</td></tr>';
        $table_exp.='<tr class="border_bottom"><td></td><td></td><td colspan="5" style="background-color:#fde59b;">Memo</td></tr>';
        $table_exp.='<tr class="border_bottom"><td colspan="7" style="background-color:#c5e1b1;"></td></tr>';
        $currentExp = '';
        while ($masterdata = mysqli_fetch_assoc($getmasterreports)) {
//            $reportsArr[] = $masterdata;
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($currentExp != $masterdata['exp_head'] && $currentExp != '') {
                $table_exp.='<tr ><td colspan="7" style="text-align:right;">=================</td></tr>';
                $exp_debitTotal = 0;
                $exp_creditTotal = 0;
            }
            $currentExp = $masterdata['exp_head'];
            if ($amt < 0) {
                $creditTotal+=abs($amt);
                $exp_creditTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['exp_head'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['ref'] . '</td><td>' . $masterdata['payee'] . '</td><td>-</td><td>' . abs($amt) . '</td><td>' . ($diff2) . '</td></tr>';
                $table_exp.='<tr class="border_bottom"><td></td><td></td><td colspan="5" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';
            } else {
                $debitTotal+=abs($amt);
                $exp_debitTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['exp_head'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['ref'] . '</td><td>' . $masterdata['payee'] . '</td><td>' . abs($amt) . '</td><td>-</td><td>' . ($diff2) . '</td></tr>';
                $table_exp.='<tr class="border_bottom"><td></td><td></td><td colspan="5" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';
            }
        }

        $getmasterreports1 = mysqli_query($conn, "SELECT * FROM `reports` WHERE `status` = 1 AND `account`!='' AND `exp_head`!='' AND `division`='" . $division . "' AND `year`='" . $year . "' ORDER BY account, date, amount");
        $exp_creditTotal = 0;
        $exp_debitTotal = 0;
        $currentExp = '';
        while ($masterdata = mysqli_fetch_assoc($getmasterreports1)) {
            $amt = str_replace(",", "", $masterdata['amount']);
            if ($currentExp != $masterdata['account'] && $currentExp != '') {
                $table_exp.='<tr ><td colspan="7" style="text-align:right;">=================</td></tr>';
                $exp_debitTotal = 0;
                $exp_creditTotal = 0;
            }
            $currentExp = $masterdata['account'];
            if ($amt < 0) {
                $debitTotal+=abs($amt);
                $exp_debitTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['account'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['ref'] . '</td><td>' . $masterdata['payee'] . '</td><td>-</td><td>' . abs($amt) . '</td><td>' . ($diff2) . '</td></tr>';
                $table_exp.='<tr class="border_bottom"><td></td><td></td><td colspan="5" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';
            } else {
                $creditTotal+=abs($amt);
                $exp_creditTotal+=abs($amt);
                $diff2 = (number_format($exp_debitTotal - $exp_creditTotal, 2));
                $diff2 = str_replace(",", "", $diff2);
                $table_exp.='<tr class="border_bottom"><td>' . $masterdata['account'] . '</td><td>' . $masterdata['date'] . '</td><td>' . $masterdata['ref'] . '</td><td>' . $masterdata['payee'] . '</td><td>' . abs($amt) . '</td><td>-</td><td>' . ($diff2) . '</td></tr>';
                $table_exp.='<tr class="border_bottom"><td></td><td></td><td colspan="5" style="background-color:#fde59b;">' . $masterdata['memo'] . '</td></tr>';
            }
        }

        if (abs($debitTotal) > 0) {
            $debitTotal = "$ " . number_format($debitTotal, 2);
        }
        if (abs($creditTotal) > 0) {
            $creditTotal = "$ " . number_format($creditTotal, 2);
        }
        $table_exp.='<tr><td colspan="4"></td><td><hr/></td><td><hr/></td><td><hr/></td></tr>';
        $table_exp.='<tr>';
        $table_exp.='<td colspan="3"> </td>';
        $table_exp.='<td>Total</td>';
        $table_exp.='<td>' . $debitTotal . '</td>';
        $table_exp.='<td>' . $creditTotal . '</td>';
        $table_exp.='<td>' . ($debitTotal - $creditTotal) . '</td>';
        $table_exp.='</tr>';
        $table_exp.='<tr><td colspan="4"></td><td><hr/></td><td><hr/></td><td><hr/></td></tr>';
        $table_exp.='</table>';
    } else {
        header("Location:detailed_table.php?fail");
    }

    $html = '<html>
                <head>
                    <style>
                        @page {
                            margin-header: 10mm;
                            margin-footer: 10mm;
                            margin:15px 15px 50px 15px;
                            
                            footer: MyFooter1;
                        }
                        body {font-family: sans-serif;
                              font-size: 10pt;
                        }
                        p {	margin: 0pt; }
                        .border_bottom td{ border-bottom:1px solid black; }
                    </style>
                </head>
                <body style="background-color: #eaeaea;">
                    <!--mpdf
                        <htmlpagefooter name="MyFooter1">
                            <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; margin-top:3mm;" >
                                Page {PAGENO} of {nb}
                            </div>
                        </htmlpagefooter>
                        <sethtmlpagefooter name="MyFooter1" value="on" />
                    mpdf-->
                    <div style="width: 100%; margin: 0; padding: 0;">
                        <div style="background-color:#fff;width:100%;">
                            ' . $table_exp . '
                        </div>
                    </div>
                </body>
            </html>';


    define('_MPDF_PATH', './mpdf60/');
    include("./mpdf60/mpdf.php");

    $mpdf = new mPDF('d', 'A4', '', '', 0, 0, 0, 0, 5, 5);
    $mpdf->SetProtection(array('print'));
    $mpdf->SetTitle($file_name . " " . date('Y-m-d'));
    $mpdf->SetAuthor("Vimal Patel");


    $mpdf->WriteHTML($html);
    $pdffile = $file_name . ".pdf";
//    $mpdf->Output();

    $mpdf->Output(PDF_PATH . $pdffile);
    header("Location:" . PDF_PATH . $pdffile);
    exit();
}