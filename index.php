<?php
include_once './header.php';
?>
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-form-title" style="background-image: url(images/bg-01.jpg);">
                <span class="login100-form-title-1">
                    &nbsp;
                </span>
            </div>
            <label class="login100-form-title-2 text-center">
                RECKON
            </label>
            <p class="alert text-success">A system for assigning the Expense Head and Calculating the Account Balances from the Bank Statements/Credit Card Statements.</p>
            <form role="form" class="login100-form validate-form" id="login_form" action="javascript:void(0)" method="post" onsubmit="login_user();">
                <input type="hidden" name="action" value="login_user">
                <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                    <span class="label-input100">Username</span>
                    <input class="input100" type="text" name="username" placeholder="Enter username" value="">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                    <span class="label-input100">Password</span>
                    <input class="input100" type="password" name="password" placeholder="Enter password" value="">
                    <span class="focus-input100"></span>
                </div>

                <!--                <div class="flex-sb-m w-full p-b-30">
                                    <div class="contact100-form-checkbox">
                                    </div>
                
                                    <div>
                                        <a href="register.php" class="txt1">
                                            New User? Register
                                        </a>
                                    </div>
                                </div>-->

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Login
                    </button>

                </div>
                <div class="footer_developed clearfix">
                    <br/>
                    <p class="text-center">Developed By <b>Vimal Patel</b>, Gujarat, India</p>
                    <p class="text-center">3<sup>rd</sup> July 2018</p>
                </div>
            </form>
            <div class="alert_message" style="display: none;"></div>


        </div>
    </div>
</div>
<?php
include_once './footer.php';
?>
